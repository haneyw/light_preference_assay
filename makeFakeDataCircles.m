function makeFakeDataCircles(mask_fname, traj_fname, vel, wall_dist)
% This function makes a fake dataset that can be used to check that
% other analysis code is working correctly. It reads in the well
% information from mask_fname and outputs a trajectory file into
% traj_fname. The trajectory is a circular trajectory with velocity
% 'vel' and wall distance 'wall_dist'. The arguments vel and wall_dist
% should be [1xN] arrays where N is the number of masks.
%
% For example:
% makeFakeDataCircles('trial0001_mask.mat', 'fake_traj.mat', ...
%    [100, 100, 20, 20, 200, 200], [15, 15, 20, 20, 30, 30]);


outlier_area = 6;
num_time_points = 10000;
num_bad_points = 100;
time_step = 1/30.0;

load(mask_fname, 'masks');
num_masks = numel(masks);

time = (0:(num_time_points-1))*time_step;
time = time';

raw_pos = zeros(2, num_masks, num_time_points);
xcorr_signal = ones(num_masks, num_time_points);

for area_idx=1:num_masks
    well_radius = masks(area_idx).params.radius;
    well_center = masks(area_idx).params.pos;
    
    traj_radius = well_radius - wall_dist(area_idx);
    omega = vel(area_idx) / traj_radius;
    
    x = well_center(1) + traj_radius .* cos(omega * time);
    y = well_center(2) + traj_radius .* sin(omega * time);
    heading = (omega * time)*180/pi + 90;
    
    
    selected_bad_points = unique(randi(num_time_points, [num_bad_points,1]));
    x(selected_bad_points) = 1e20;
    y(selected_bad_points) = 1e20;
    heading(selected_bad_points) = 1e20;
    
    raw_pos(1,area_idx,:) = x;
    raw_pos(2,area_idx,:) = y;
    raw_heading(area_idx,:) = heading;
    xcorr_signal(area_idx, selected_bad_points) = 0.0;
end

raw_pos(1:2,outlier_area,:) = 1e20;
raw_heading(area_idx,:) = 1e20;

save(traj_fname, 'time','raw_pos', 'raw_heading', 'xcorr_signal'); 

end