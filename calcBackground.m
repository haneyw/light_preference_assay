function calcBackground(date_name)
% This function accepts the following inputs
%   date_name - string that is the name of the folder you wish to calculate
%               background movie for.
% 
% The purpose of this function is to gather the UFMF files for a specific
%   date and to run lpgCalculateBackgoundMovie to create a changing
%   background movie. 

% set the window size that you want to average across and the number of
% frames to skip between each sample.
bkg_cfg.quantile = 0.9;
bkg_cfg.window_size = 51;
bkg_cfg.frame_skipping = 1000;

raw_data_folder = [getDataFolder(), filesep, ...
    'raw_data', filesep, date_name];
analysis_folder = [getDataFolder(), filesep, ...
    'analysis', filesep, date_name];

if ~exist(analysis_folder,'dir')
    mkdir(analysis_folder);
end

exper_params = getExperimentParams();

for idx=1:size(exper_params,1)    
    if ~strcmp(exper_params{idx,1}, date_name)
        continue;
    end
    
    trial_num = exper_params{idx,2};
    protocol_name = exper_params{idx,4};
    
    props = getPlateProps(protocol_name);

	% add stage to filename if necessary
    if props.num_stages == 1
       vid_fname = [raw_data_folder, filesep, ...
           sprintf('trial%04d.ufmf', trial_num)];
       bkg_fname = [analysis_folder, filesep, ...
           sprintf('trial%04d_bkg.ufmf', trial_num)];
      
       lpg_calculateBackgroundMovie(vid_fname, bkg_fname, bkg_cfg);

    else
       for stage_idx=1:props.num_stages
          vid_fname = [raw_data_folder, filesep, ...
              sprintf('trial%04d_stage%02d.ufmf', trial_num, stage_idx)];
          bkg_fname = [analysis_folder, filesep, ...
              sprintf('trial%04d_stage%02d_bkg.ufmf', trial_num, stage_idx)];
          
          lpg_calculateBackgroundMovie(vid_fname, bkg_fname, bkg_cfg);
       end
    end
end

end
