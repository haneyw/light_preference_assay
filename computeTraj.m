function computeTraj(date_name)
% This function accepts the following inputs
%   date_name - string that is the name of the folder you wish to calculate
%               trajectories for.

raw_data_folder = [getDataFolder(), filesep, ...
    'raw_data', filesep, date_name];
analysis_base_folder = [getDataFolder(), filesep, ...
    'analysis'];
analysis_folder = [analysis_base_folder, filesep, date_name];

if ~exist(analysis_folder,'dir')
    mkdir(analysis_folder);
end

exper_params = getExperimentParams();

for idx=1:size(exper_params,1)    
    if ~strcmp(exper_params{idx,1}, date_name)
        continue;
    end

    trial_num = exper_params{idx,2};
    protocol_name = exper_params{idx,4};

    props = getPlateProps(protocol_name);

    % add stage to filename if necessary
    if props.num_stages == 1
        vid_fname = [raw_data_folder, filesep, ...
            sprintf('trial%04d.ufmf', trial_num)];
        bkg_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_bkg.ufmf', trial_num)];
        mask_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_mask.mat', trial_num)];
        heartm_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_heart_mask.mat', trial_num)];
        heartb_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_heart_beat.mat', trial_num)];
        ref_set_fname = [analysis_base_folder, filesep, ...
            'ref_set.mat'];
        traj_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_traj.mat', trial_num)];
        
        lpg_getLarvaTrajectoryPattern(vid_fname, bkg_fname, mask_fname, ...
            ref_set_fname, traj_fname);

        if ~strcmp(props.heartbeat_type{1}, 'none')
            lpg_computeHeartbeat(vid_fname, heartm_fname, heartb_fname);
        end
    else
        for stage_idx=1:props.num_stages
            vid_fname = [raw_data_folder, filesep, ...
                sprintf('trial%04d_stage%02d.ufmf', trial_num, stage_idx)];
            bkg_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_bkg.ufmf', trial_num, stage_idx)];
            mask_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_mask.mat', trial_num, stage_idx)];
            heartm_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_heart_mask.mat', trial_num, stage_idx)];
            heartb_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_heart_beat.mat', trial_num, stage_idx)];
            ref_set_fname = [analysis_base_folder, filesep, ...
                'ref_set.mat'];
            traj_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_traj.mat', trial_num, stage_idx)];
            
            lpg_getLarvaTrajectoryPattern(vid_fname, bkg_fname, mask_fname, ...
                ref_set_fname, traj_fname);

            if ~strcmp(props.heartbeat_type{stage_idx}, 'none')
                lpg_computeHeartbeat(vid_fname, heartm_fname, heartb_fname);
            end
        end    
    end
 
end

end
