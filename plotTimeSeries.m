function plotTimeSeries(figure_type)
% This function scans the dataset, aggregates values, and produces
% figures.

mm_to_pixels = 12.5950;  %see notebook 1/16/19
trans_speed_ylim = [0, 4];
ang_speed_ylim = [0, 1400];
wall_dist_ylim = [0, 35/2];
area_use_ylim = [0, 20];
switch_ylim = [0, 0.2];
explori_ylim = [0, 1];
preference_ylim = [-1.1, 1.1];
tseries_window_period = 60;

output_folder = '/home/lmember/Documents/AustinHaney/figures';

switch figure_type
    case 'top_bottom_left_vs_right'
        % This case plots time series for the normal condition
        % left/right sides, and makes boxplots comparing the two
        protocol_list = {'top_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
    case 'top_bottom_vs_open_field'
        % This case plots time series for the top/bottom experiments,
        % comparing against the open field condition
        protocol_list = {'top_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
        
    case 'top_bottom_vs_age'
        % This case plots time series for the top/bottom experiments,
        % comparing the ages.
        
        error 'set dpf to reference age manually in for loops below';
        
        protocol_list = {'top_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = [4,5,6];
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
        
    case 'no_feed_left_vs_right'
        % This case plots time series for the no-feed condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'no_feed'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
    case 'no_feed_vs_normal'
        % This case plots time series for the no-feed condition,
        % comparing against the fed condition        
        protocol_list = {'no_feed'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'size_55mm_left_vs_right'
        % This case plots time series for the 55mm dish condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'size_55mm_control', 'size_55mm_top', 'size_55mm_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
        
        wall_dist_ylim = [0, 55/2];
        area_use_ylim = [0, 50];
    case 'size_55mm_vs_normal'
        % This case plots time series for the 55mm condition,
        % comparing against the normal condition        
        protocol_list = {'size_55mm_control', 'size_55mm_top', 'size_55mm_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

        wall_dist_ylim = [0, 55/2];
        area_use_ylim = [0, 50];
 
    case 'size_75mm_left_vs_right'
        % This case plots time series for the 75mm dish condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'size_75mm_control', 'size_75mm_top', 'size_75mm_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
        
        wall_dist_ylim = [0, 75/2];
        area_use_ylim = [0, 90];
    case 'size_75mm_vs_normal'
        % This case plots time series for the 75mm condition,
        % comparing against the normal condition        
        protocol_list = {'size_75mm_control', 'size_75mm_top', 'size_75mm_bottom'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];
        
        wall_dist_ylim = [0, 75/2];
        area_use_ylim = [0, 90];
        
    case 'AITC_left_vs_right_prestim'
        % This case plots time series for the AITC condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [0, 120, 360, 480, 960, 1080];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 120];
        
        ref_protocol_list = [];
        ref_stage_nums = [1,2];
        ref_comp_type = 'left_right';
        
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 120];
        
    case 'AITC_left_vs_right_init'
        % This case plots time series for the AITC condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [0, 120, 360, 480, 960, 1080];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [360, 120];
        
        ref_protocol_list = [];
        ref_stage_nums = [1,2];
        ref_comp_type = 'left_right';
        
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [360, 120];
        
    case 'AITC_left_vs_right_long'
        % This case plots time series for the AITC condition
        % the left/right sides, and makes boxplots comparing the two
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [0, 120, 360, 480, 960, 1080];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [960, 120];
        
        ref_protocol_list = [];
        ref_stage_nums = [1,2];
        ref_comp_type = 'left_right';
        
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [960, 120];
        
    case 'AITC_init_vs_prestim'
        % This case plots time series for the AITC condition,
        % comparing the initial response against the prestimulus condition
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [360, 480, 960, 1260];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [360, 120];
        
        ref_protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
        
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 120];
        
    case 'AITC_long_vs_prestim'
        % This case plots time series for the AITC condition,
        % comparing the long term response against the prestimulus condition
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [360, 480, 960, 1260];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [960, 300];
        
        ref_protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'AITC_long_vs_init'
        % This case plots time series for the AITC condition,
        % comparing the long term response against the initial response        
        protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        stage_nums = [1,2];
        mark_times = [360, 480, 960, 1260];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [960, 120];
        
        ref_protocol_list = {'bottom_AITC', 'control_AITC', 'top_AITC'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [360, 120];
        
    case 'AITC_white_long_vs_prestim'
        % This case plots time series for the AITC_white condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'top_bottom_AITC_white'};
        stage_nums = [1,2];
        mark_times = [360, 480, 960, 1260];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [960, 300];
        
        ref_protocol_list = {'top_bottom_AITC_white'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'AITC_black_long_vs_prestim'
        % This case plots time series for the AITC_black condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'top_bottom_AITC_black'};
        stage_nums = [1,2];
        mark_times = [360, 480, 960, 1260];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [960, 300];
        
        ref_protocol_list = {'top_bottom_AITC_black'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'AITC_meto_long_vs_prestim'
        % This case plots time series for the AITC_meto condition,
        % comparing the long term response against the prestimulus
        % condition. These experiments did not have strong controls
        % and should not be used for publication.
        protocol_list = {'AITC_meto'};
        stage_nums = [1,2,3];
        mark_times = [660, 3420, 4020, 4320];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [4020, 300];
        
        ref_protocol_list = {'AITC_meto'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'AITC_pb_long_vs_prestim'
        % This case plots time series for the AITC_pb condition,
        % comparing the long term response against the prestimulus
        % condition. These experiments did not have strong controls
        % and should not be used for publication.
        protocol_list = {'AITC_pb'};
        stage_nums = [1,2,3];
        mark_times = [660, 3420, 4020, 4320];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [4020, 300];
        
        ref_protocol_list = {'AITC_pb'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'AITC_rb_long_vs_prestim'
        % This case plots time series for the AITC red/black condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'AITC_rb'};
        stage_nums = [1,2];
        mark_times = [360, 1560, 1860];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1560, 300];
        
        ref_protocol_list = {'AITC_rb'};
        ref_stage_nums = [1,2];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];

    case 'eshock_3ma_long_vs_prestim'
        % This case plots time series for the AITC eshock_3ma condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'eshock_3ma'};
        stage_nums = [1];
        mark_times = [-180, 0, 1200, 1380];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1200, 180];
        
        ref_protocol_list = {'eshock_3ma'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [-180, 180];
        
    case 'eshock_6ma_long_vs_prestim'
        % This case plots time series for the AITC eshock_6ma condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'eshock_6ma'};
        stage_nums = [1];
        mark_times = [-180, 0, 1200, 1380];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1200, 180];
        
        ref_protocol_list = {'eshock_6ma'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [-180, 180];
        
    case 'eshock_6ma_left_vs_right'
        % This case plots time series for eshock 6ma condition
        % left/right sides, and makes boxplots comparing the two
        protocol_list = {'eshock_6ma'};
        stage_nums = 1;
        mark_times = [-180, 0, 1200, 1380];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1200, 180];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
        
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [1200, 180];
        
    case 'bottom_proj_vs_normal'
        % This case plots time series for the bottom_proj experiments,
        % comparing against the open field condition
        protocol_list = {'bottom_proj'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'bottom_proj_overhead_vs_normal'
        % This case plots time series for the bottom_proj_overhead experiments,
        % comparing against the open field condition
        protocol_list = {'bottom_proj_overhead'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'bottom_proj_v2_vs_normal'
        % This case plots time series for the bottom_proj_v2 experiments,
        % comparing against the open field condition
        protocol_list = {'bottom_proj_v2'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'bottom_proj_rb_vs_normal'
        % This case plots time series for the bottom_proj_rb experiments,
        % comparing against the open field condition
        protocol_list = {'bottom_proj_rb'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = {'top_bottom'};
        ref_stage_nums = 1;
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'bottom_proj_rb_left_vs_right'
        % This case plots time series for the normal condition
        % left/right sides, and makes boxplots comparing the two
        protocol_list = {'bottom_proj_rb'};
        stage_nums = 1;
        mark_times = [0, 1500];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [0, 1500];
        
        ref_protocol_list = [];
        ref_stage_nums = 1;
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 1500];

    case 'flash_long_vs_prestim'
        % This case plots time series for the flash condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash'};
        stage_nums = [1];
        mark_times = [300, 1500, 1800];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1500, 300];
        
        ref_protocol_list = {'flash'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'flash_20sec_long_vs_prestim'
        % This case plots time series for the flash_20sec condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_20sec'};
        stage_nums = [1];
        mark_times = [300, 1500, 1800];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [1500, 300];
        
        ref_protocol_list = {'flash_20sec'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];

    case 'flash_30sec_4min_long_vs_prestim'
        % This case plots time series for the flash_30sec_4min condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_30sec_4min'};
        stage_nums = [1];
        mark_times = [300, 3000, 3300];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = 30;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [3000, 300];
        
        ref_protocol_list = {'flash_30sec_4min'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'flash_2min_2min_long_vs_prestim'
        % This case plots time series for the flash_2min_2min condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_2min'};
        stage_nums = [1];
        mark_times = [300, 2700, 3000];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [2700, 300];
        
        ref_protocol_list = {'flash_2min_2min'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];

    case 'flash_2min_8min_long_vs_prestim'
        % This case plots time series for the flash_2min_8min condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_8min'};
        stage_nums = [1];
        mark_times = [300, 6300, 6600];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [6300, 300];
        
        ref_protocol_list = {'flash_2min_8min'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];

    case 'flash_2min_8min_lf_long_vs_prestim'
        % This case plots time series for the flash_2min_8min_lf condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_8min_lf'};
        stage_nums = [1];
        mark_times = [300, 6300, 6600];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [6300, 300];
        
        ref_protocol_list = {'flash_2min_8min_lf'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];

    case 'flash_2min_8min_rb_left_vs_right'
        % This case plots time series for the flash_2min_8min_rb condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_8min_rb'};
        stage_nums = [1];
        mark_times = [6300, 6900];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'separate';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [6300, 600];
        
        ref_protocol_list = [];
        ref_stage_nums = [1];
        ref_comp_type = 'left_right';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [6300, 600];
        
    case 'flash_2min_8min_rb_long_vs_prestim'
        % This case plots time series for the flash_2min_8min_rb condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_8min_rb'};
        stage_nums = [1];
        mark_times = [300, 480, 780, 6300, 6600];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [6300, 300];
        
        ref_protocol_list = {'flash_2min_8min_rb'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [0, 300];
        
    case 'flash_2min_8min_rb_long_vs_init'
        % This case plots time series for the flash_2min_8min_rb condition,
        % comparing the long term response against the prestimulus condition        
        protocol_list = {'flash_2min_8min_rb'};
        stage_nums = [1];
        mark_times = [300, 480, 780, 6300, 6600];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [6300, 300];
        
        ref_protocol_list = {'flash_2min_8min_rb'};
        ref_stage_nums = [1];
        ref_comp_type = 'ref_respective';
                
        bplot_ref_is_paired = true;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [480, 300];

    case 'AITC_drugs_long_vs_normal'
        % This case plots time series for the AITC_drugs condition,
        % comparing the long term response against the control condition        
        protocol_list = {'AITC_drugs'};
        stage_nums = [1,2,3];
        mark_times = [660, 3420, 4020, 4320];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [4020, 300];
        
        ref_protocol_list = {'AITC_drugs'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [4020, 300];
        
    case 'AITC_drugs2_long_vs_normal'
        % This case plots time series for the AITC_drugs2 condition,
        % comparing the long term response against the control condition        
        protocol_list = {'AITC_drugs2'};
        stage_nums = [1,2,3];
        mark_times = [660, 3060, 3420, 4020, 4920, 5040];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [4020, 120];
        
        ref_protocol_list = {'AITC_drugs2'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [4020, 120];
        
    case 'AITC_drugs2_verylong_vs_normal'
        % This case plots time series for the AITC_drugs2 condition,
        % comparing the very long term response against the control condition        
        protocol_list = {'AITC_drugs2'};
        stage_nums = [1,2,3];
        mark_times = [660, 3060, 3420, 4020, 4920, 5040];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [4920, 120];
        
        ref_protocol_list = {'AITC_drugs2'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [4920, 120];
        
    case 'AITC_drugs2_prior_vs_normal'
        % This case plots time series for the AITC_drugs2 condition,
        % comparing the very long term response against the control condition        
        protocol_list = {'AITC_drugs2'};
        stage_nums = [1,2,3];
        mark_times = [660, 3060, 3420, 4020, 4920, 5040];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [3060, 120];
        
        ref_protocol_list = {'AITC_drugs2'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [3060, 120];
        
    case 'AITC_drugs2_init_vs_normal'
        % This case plots time series for the AITC_drugs2 condition,
        % comparing the very long term response against the control condition        
        protocol_list = {'AITC_drugs2'};
        stage_nums = [1,2,3];
        mark_times = [660, 3060, 3420, 4020, 4920, 5040];
        
        dpf_list = 7;
        
        tseries_subregion_handling = 'combine';
        tseries_window_method.type = 'period';
        tseries_window_method.period = tseries_window_period;
        tseries_window_method.times = [];
        
        bplot_window_method.type = 'times';
        bplot_window_method.period = [];
        bplot_window_method.times = [3420, 120];
        
        ref_protocol_list = {'AITC_drugs2'};
        ref_stage_nums = [1,2,3];
        ref_comp_type = 'ref_control';
                
        bplot_ref_is_paired = false;
        bplot_ref_window_method.type = 'times';
        bplot_ref_window_method.period = [];
        bplot_ref_window_method.times = [3420, 120];
        
    otherwise
        error 'unrecognized figure type';
end



% Time Series
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for dpf= dpf_list
    name_suffix = sprintf(': %ddpf', dpf);
    
    grand_accum = aggregateTrials(dpf+[-0.1,0.1], stage_nums, protocol_list, ...
        tseries_window_method);
    
    if ~isempty(ref_protocol_list)
        ref_grand_accum = aggregateTrials(dpf+[-0.1,0.1], ref_stage_nums, ref_protocol_list, ...
            tseries_window_method);
    else
        ref_grand_accum = [];
    end
    
    f = figure('Position', [300, 300, 1200, 1200], ...
        'Color', 'w');
    
    set(f, 'Renderer', 'painters');    
    
    subpanel = uipanel('Parent', f, 'Position', [0.0, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, 1/mm_to_pixels, ...
        'trans_vel', false, trans_speed_ylim, ['Swimming Speed (mm/s)', name_suffix], ...
        tseries_subregion_handling, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [1/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, 180/pi, ...
        'ang_vel', false, ang_speed_ylim, ['Turning Speed (deg/s)', name_suffix], ...
        tseries_subregion_handling, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [2/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, 1/mm_to_pixels, ...
        'wall_dist', false, wall_dist_ylim, ['Wall Distance (mm)', name_suffix], ...
        tseries_subregion_handling, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [3/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, (1/mm_to_pixels)^2, ...
        'area_use', false, area_use_ylim, ['Area Use (mm2,s)', name_suffix], ...
        tseries_subregion_handling, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [0.0, 0.0, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, 1.0, ...
        'switch_rate', true, switch_ylim, ['Switch Rate (1/s)', name_suffix], ...
        'use_first', ref_comp_type, subpanel); % switch rate is placed in first subregion
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [1/4, 0.0, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesPreference(grand_accum, ref_grand_accum, ...
        'total_time', true, preference_ylim, ['Indiv. Preference', name_suffix], ...
        true, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [2/4, 0.0, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesPreference(grand_accum, ref_grand_accum, ...
        'total_time', true, preference_ylim, ['Preference', name_suffix], ...
        false, ref_comp_type, subpanel);
    addEventMarks(ax_list, mark_times);
    
    subpanel = uipanel('Parent', f, 'Position', [3/4, 0.0, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, 1.0, ...
        'explor_index', true, explori_ylim, ['Exploration Index', name_suffix], ...
        'use_first', ref_comp_type, subpanel); % exploration index is placed in first subregion
    addEventMarks(ax_list, mark_times);

    fname = sprintf('%s_%ddpf.eps', figure_type, dpf);
    full_fname = [output_folder, filesep, fname];
    print(full_fname, '-Ppainters', '-depsc');
    
    pause(5);
    close(f);
end


% Bar Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for dpf= dpf_list
    name_suffix = sprintf(': %ddpf', dpf);
    
    grand_accum = aggregateTrials(dpf+[-0.1,0.1], stage_nums, protocol_list, ...
        bplot_window_method);
    
    if ~isempty(ref_protocol_list)
        ref_grand_accum = aggregateTrials(dpf+[-0.1,0.1], ref_stage_nums, ref_protocol_list, ...
            bplot_ref_window_method);
    else
        ref_grand_accum = [];
    end
    
    f = figure('Position', [300, 300, 1200, 800], ...
        'Color', 'w');
    
    set(f, 'Renderer', 'painters');  
    
    subpanel = uipanel('Parent', f, 'Position', [0.0, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotBarsAbsolute(grand_accum, ref_grand_accum, 1/mm_to_pixels, ...
        'trans_vel', false, trans_speed_ylim, ['Swimming Speed (mm/s)', name_suffix], ...
        bplot_ref_is_paired, false, ref_comp_type, subpanel);
    
    subpanel = uipanel('Parent', f, 'Position', [1/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotBarsAbsolute(grand_accum, ref_grand_accum, 180/pi, ...
        'ang_vel', false, ang_speed_ylim, ['Turning Speed (deg/s)', name_suffix], ...
        bplot_ref_is_paired, false, ref_comp_type, subpanel);
    
    subpanel = uipanel('Parent', f, 'Position', [2/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotBarsAbsolute(grand_accum, ref_grand_accum, 1/mm_to_pixels, ...
        'wall_dist', false, wall_dist_ylim, ['Wall Distance (mm)', name_suffix], ...
        bplot_ref_is_paired, false, ref_comp_type, subpanel);
    
    subpanel = uipanel('Parent', f, 'Position', [3/4, 1/2, 1/4, 1/2], ...
        'BorderType', 'none', 'BackgroundColor', 'w');
    ax_list = plotBarsAbsolute(grand_accum, ref_grand_accum, (1/mm_to_pixels)^2, ...
        'area_use', false, area_use_ylim, ['Area Use (mm2,s)', name_suffix], ...
        bplot_ref_is_paired, false, ref_comp_type, subpanel);
    
    if ~strcmp(ref_comp_type, 'left_right')
    
        subpanel = uipanel('Parent', f, 'Position', [0.0, 0.0, 1/4, 1/2], ...
            'BorderType', 'none', 'BackgroundColor', 'w');
        ax_list = plotBarsAbsolute(grand_accum, ref_grand_accum, 1.0, ...
            'switch_rate', true, switch_ylim, ['Switch Rate (1/s)', name_suffix], ...
            bplot_ref_is_paired, false, ref_comp_type, subpanel);

        subpanel = uipanel('Parent', f, 'Position', [1/4, 0.0, 1/4, 1/2], ...
            'BorderType', 'none', 'BackgroundColor', 'w');
        ax_list = plotBarsPreference(grand_accum, ref_grand_accum, ...
            'total_time', true, preference_ylim, ['Indiv. Preference', name_suffix], true, ...
            bplot_ref_is_paired, false, ref_comp_type, subpanel);

        subpanel = uipanel('Parent', f, 'Position', [2/4, 0.0, 1/4, 1/2], ...
            'BorderType', 'none', 'BackgroundColor', 'w');
        ax_list = plotBarsPreference(grand_accum, ref_grand_accum, ...
            'total_time', true, preference_ylim, ['Preference', name_suffix], false, ...
            bplot_ref_is_paired, false, ref_comp_type, subpanel);

        subpanel = uipanel('Parent', f, 'Position', [3/4, 0.0, 1/4, 1/2], ...
            'BorderType', 'none', 'BackgroundColor', 'w');
        ax_list = plotBarsExplorationIndex(grand_accum, ref_grand_accum, ...
            'total_time', true, explori_ylim, ['Exploration Index', name_suffix], ...
            bplot_ref_is_paired, false, ref_comp_type, subpanel);
        
    end
    
    fname = sprintf('%s_%ddpf_bplot.eps', figure_type, dpf);
    full_fname = [output_folder, filesep, fname];
    print(full_fname, '-Ppainters', '-depsc');
    
    pause(5);
    close(f);
end


end

function addEventMarks(ax_list, mark_times)

mark_times = mark_times / 60; % sec to minutes

for ax_idx=1:length(ax_list)
    ax = ax_list{ax_idx};
    
    if isempty(ax)
        continue;
    end
    
    hold(ax, 'on');
    
    for time_idx=1:length(mark_times)
        plot(ax, repmat(mark_times(time_idx), 1, 2), ax.YLim, 'm');
    end
    
    hold(ax, 'off');
end

end


function ax = plotBarsAbsolute(grand_accum, ref_grand_accum, ...
    scale_factor, field_name, use_mean, yrange, yname, ref_is_paired, ...
    ref_is_nonpara, ref_comp_type, parent)

% note: ref_grand_accum should not be empty unless ref_comp_type
% is equal to 'left_right'

disp_list = {'R:Ctrl', 'E:Ctrl', 'R:Bot', 'E:Bot', 'R:Top', 'E:Top'};
setup_name_list = {'control', 'bot_shade', 'top_shade'};

ax = axes(parent);

plot_vals_accum = [];
plot_group_accum = [];

for setup_idx=1:3
    setup_name = setup_name_list{setup_idx};
    
    if isempty(grand_accum.(setup_name))
        continue;
    end 
    
    for ref_expr_idx=0:1
        
        switch ref_comp_type
            case 'left_right'
                local_accum = grand_accum.(setup_name);
                if isempty(local_accum)
                    continue;
                end
                
                if ref_expr_idx==0
                    vals = [local_accum(:,1).(field_name)]';
                else
                    vals = [local_accum(:,2).(field_name)]';
                end 
            case 'ref_control'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.control;
                else
                    local_accum = grand_accum.(setup_name);
                end
                
                if isempty(local_accum)
                    continue;
                end
                
                left_vals = [local_accum(:,1).(field_name)]';
                left_time = [local_accum(:,1).total_time]';
                right_vals = [local_accum(:,2).(field_name)]';
                right_time = [local_accum(:,2).total_time]';
                
                vals = combineLeftRight(left_vals, left_time, ...
                    right_vals, right_time);
            case 'ref_respective'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.(setup_name);
                else
                    local_accum = grand_accum.(setup_name);
                end
                
                if isempty(local_accum)
                    continue;
                end
                
                left_vals = [local_accum(:,1).(field_name)]';
                left_time = [local_accum(:,1).total_time]';
                right_vals = [local_accum(:,2).(field_name)]';
                right_time = [local_accum(:,2).total_time]';
                
                vals = combineLeftRight(left_vals, left_time, ...
                    right_vals, right_time);
            otherwise
                error 'unrecognized comp type';
        end
        
        % NOTE: group ordering must match disp_list
        group_idx = 2*(setup_idx-1) + ref_expr_idx + 1;
        
        vals = vals * scale_factor;
        num_elem = length(vals);

        plot_vals_accum = [plot_vals_accum; vals];
        plot_group_accum = [plot_group_accum; group_idx * ones(num_elem,1)];
    end
    
end

used_group_ids = unique(plot_group_accum);
[~,new_group_ids] = ismember(plot_group_accum, used_group_ids);
new_labels = disp_list(used_group_ids);

stat_test.mode = 'adjacent';
if ref_is_paired
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_paired';
    else
        stat_test.type = 'ttest_paired';
    end
else
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_unpaired';
    else
        stat_test.type = 'ttest_unpaired';
    end
end

plotBarsWithSem(ax, plot_vals_accum, new_group_ids, new_labels, ...
    stat_test, use_mean, yrange);

if ~isempty(yrange)
    ylim(ax, yrange);
end

ylabel(ax, yname);

ax.XTickLabelRotation = 90;

end


function ax = plotBarsPreference(grand_accum, ref_grand_accum, ...
    field_name, use_mean, yrange, yname, norm_indiv, ref_is_paired, ...
    ref_is_nonpara, ref_comp_type, parent)
        
% note: ref_grand_accum should not be empty unless ref_comp_type
% is equal to 'left_right'

disp_list = {'R:Ctrl', 'E:Ctrl', 'R:Bot', 'E:Bot', 'R:Top', 'E:Top'};
setup_name_list = {'control', 'bot_shade', 'top_shade'};

ax = axes(parent);

plot_vals_accum = [];
plot_group_accum = [];

for setup_idx=1:3
    setup_name = setup_name_list{setup_idx};
   
    if isempty(grand_accum.(setup_name))
        continue;
    end 
    
    for ref_expr_idx=0:1
    
        switch ref_comp_type
            case 'left_right'
                error('internal error');
            case 'ref_control'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.control;
                else
                    local_accum = grand_accum.(setup_name);
                end
            case 'ref_respective'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.(setup_name);
                else
                    local_accum = grand_accum.(setup_name);
                end
            otherwise
                error 'unrecognized comp type';
        end

        if isempty(local_accum)
            continue;
        end
        
        left_vals = [local_accum(:,1).(field_name)]';
        right_vals = [local_accum(:,2).(field_name)]';
        
        weird_mask = xor(isnan(left_vals), isnan(right_vals));
        if nnz(weird_mask) > 0
            error 'found invalid data';
        end
        
        vals = (left_vals - right_vals) ./ (left_vals + right_vals);
        
        group_idx = 2*(setup_idx-1) + ref_expr_idx + 1;

        if norm_indiv
            indiv_sign = sign(mean(vals, 2, 'omitnan'));
            vals = bsxfun(@times, vals, indiv_sign);
        end

        num_elem = length(vals);

        plot_vals_accum = [plot_vals_accum; vals];
        plot_group_accum = [plot_group_accum; group_idx * ones(num_elem,1)];
    end
end

used_group_ids = unique(plot_group_accum);
[~,new_group_ids] = ismember(plot_group_accum, used_group_ids);
new_labels = disp_list(used_group_ids);

stat_test.mode = 'adjacent';
if ref_is_paired
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_paired';
    else
        stat_test.type = 'ttest_paired';
    end
else
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_unpaired';
    else
        stat_test.type = 'ttest_unpaired';
    end
end

plotBarsWithSem(ax, plot_vals_accum, new_group_ids, new_labels, ...
    stat_test, use_mean, yrange);

if ~isempty(yrange)
    ylim(ax, yrange);
end

ylabel(ax, yname);

ax.XTickLabelRotation = 90;

end


function ax = plotBarsExplorationIndex(grand_accum, ref_grand_accum, ...
    field_name, use_mean, yrange, yname, ref_is_paired, ...
    ref_is_nonpara, ref_comp_type, parent)

% note: ref_grand_accum should not be empty unless ref_comp_type
% is equal to 'left_right'

disp_list = {'R:Ctrl', 'E:Ctrl', 'R:Bot', 'E:Bot', 'R:Top', 'E:Top'};
setup_name_list = {'control', 'bot_shade', 'top_shade'};

ax = axes(parent);

plot_vals_accum = [];
plot_group_accum = [];

for setup_idx=1:3
    setup_name = setup_name_list{setup_idx};
    
    if isempty(grand_accum.(setup_name))
        continue;
    end 
    
    for ref_expr_idx=0:1
    
        switch ref_comp_type
            case 'left_right'
                error('internal error');
            case 'ref_control'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.control;
                else
                    local_accum = grand_accum.(setup_name);
                end
            case 'ref_respective'
                if ref_expr_idx==0
                    local_accum = ref_grand_accum.(setup_name);
                else
                    local_accum = grand_accum.(setup_name);
                end
            otherwise
                error 'unrecognized comp type';
        end

        if isempty(local_accum)
            continue;
        end
        
        
        left_vals = [local_accum(:,1).(field_name)]';
        right_vals = [local_accum(:,2).(field_name)]';
        
        weird_mask = xor(isnan(left_vals), isnan(right_vals));
        if nnz(weird_mask) > 0
            error 'found invalid data';
        end

        p_on_left = left_vals ./ (left_vals + right_vals);
        p_on_left(p_on_left < 1e-4) = 1e-4;
        p_on_left(p_on_left > 1-1e-4) = 1-1e-4;
        
        vals = 1 - abs(1 - 2*p_on_left);

        group_idx = 2*(setup_idx-1) + ref_expr_idx + 1;

        num_elem = length(vals);

        plot_vals_accum = [plot_vals_accum; vals];
        plot_group_accum = [plot_group_accum; group_idx * ones(num_elem,1)];
    end
end

used_group_ids = unique(plot_group_accum);
[~,new_group_ids] = ismember(plot_group_accum, used_group_ids);
new_labels = disp_list(used_group_ids);

stat_test.mode = 'adjacent';
if ref_is_paired
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_paired';
    else
        stat_test.type = 'ttest_paired';
    end
else
    if ref_is_nonpara
        stat_test.type = 'wilcoxon_unpaired';
    else
        stat_test.type = 'ttest_unpaired';
    end
end

plotBarsWithSem(ax, plot_vals_accum, new_group_ids, new_labels, ...
    stat_test, use_mean, yrange);

if ~isempty(yrange)
    ylim(ax, yrange);
end

ylabel(ax, yname);

ax.XTickLabelRotation = 90;

end


function plotBarsWithSem(ax, vals, groups, labels, stat_test, use_mean, yrange)

num_groups = max(groups);

mean_vals = zeros(num_groups,1);
sem_vals = zeros(num_groups,1);
for idx=1:num_groups
    mask = ~isnan(vals) & groups==idx;
    
    if use_mean
        mean_vals(idx) = mean(vals(mask));
    else
        mean_vals(idx) = median(vals(mask));
    end
    
    sem_vals(idx) = std(vals(mask))/sqrt(nnz(mask));
end

hold(ax,'on');
h = bar(ax, mean_vals);
set(h, 'FaceColor', 'w', 'LineWidth', 2);

for idx=1:num_groups
    h = errorbar(ax, idx, mean_vals(idx), sem_vals(idx));
    set(h, 'Color', 'k', 'LineWidth', 2);
end

hold(ax, 'off');

xlim(ax, [0, num_groups+1]);
set(ax, 'XTick', 1:num_groups);
set(ax, 'XTickLabel', labels);

switch stat_test.mode
    case 'none'
        % do nothing
    case 'adjacent'
        if mod(num_groups,2) ~= 0
            error 'num groups must be factor of 2';
        end
        
        for run_idx=1:2:num_groups

            switch stat_test.type
                case 'ttest_paired'
                    grpA_vals = vals(groups == run_idx);
                    grpB_vals = vals(groups == run_idx+1);
                    
                    mask = isnan(grpA_vals) | isnan(grpB_vals);
                    grpA_vals(mask) = [];
                    grpB_vals(mask) = [];
                    
                    [~,p] = ttest(grpA_vals, grpB_vals);
                case 'wilcoxon_paired'
                    grpA_vals = vals(groups == run_idx);
                    grpB_vals = vals(groups == run_idx+1);
                    
                    mask = isnan(grpA_vals) | isnan(grpB_vals);
                    grpA_vals(mask) = [];
                    grpB_vals(mask) = [];
                    
                    p = signrank(grpA_vals, grpB_vals);
                case 'ttest_unpaired'
                    grpA_vals = vals(~isnan(vals) & groups == run_idx);
                    grpB_vals = vals(~isnan(vals) & groups == run_idx+1);
                    
                    [~,p] = ttest2(grpA_vals, grpB_vals);
                case 'wilcoxon_unpaired'
                    grpA_vals = vals(~isnan(vals) & groups == run_idx);
                    grpB_vals = vals(~isnan(vals) & groups == run_idx+1);
                    
                    p = ranksum(grpA_vals, grpB_vals);
                otherwise
                    error 'unknown stat test';
            end
            
            p_txt = pValToStars(p);
            
            if ~isempty(p_txt)

                % make positions fraction of total height
                x_pos = run_idx + 0.5;
                y_pos = yrange(2) - 0.03 * (yrange(2)-yrange(1));
                y_height = 0.02 * (yrange(2)-yrange(1));
                
                hold(ax, 'on');
                h = plot(ax, ...
                    [run_idx, run_idx, run_idx+1, run_idx+1], ...
                    [y_pos - y_height, y_pos, y_pos, y_pos-y_height]);
                set(h, 'Color', 'k', 'LineWidth', 1.5);
                
                h = text(ax, x_pos, y_pos + y_height, p_txt);
                set(h, ...
                    'HorizontalAlignment', 'center', ...
                    'FontSize', 11);

                %set(h, 'Rotation', 90);
                
                hold(ax, 'off');
            end
        end
        
    otherwise
        error 'unrecognized stat test';
end

end

function txt = pValToStars(p)

if p < 0.001
    txt = '***';
elseif p < 0.01
    txt = '**';
elseif p < 0.05
    txt = '*';
else
    txt = [];
end

%txt = [txt, '(', num2str(p), ')'];

end


function y = combineLeftRight(left_vals, left_time, right_vals, right_time)
% This function combines the values from the left and the right sides,
% weighting by the time spent on each side. Any NaN values are ignored
% when performing the sum, unless both left and right are NaN, in which
% case the returned value is a NaN.

left_nan_mask = isnan(left_vals);
right_nan_mask = isnan(right_vals);
both_nan_mask = left_nan_mask & right_nan_mask;

left_vals(left_nan_mask & ~both_nan_mask) = 0;
left_time(left_nan_mask & ~both_nan_mask) = 0;

right_vals(right_nan_mask & ~both_nan_mask) = 0;
right_time(right_nan_mask & ~both_nan_mask) = 0;

denom = left_time + right_time;
denom(denom == 0) = 1;

y = (left_vals .* left_time + right_vals .* right_time) ./ denom;


end


function ax_list = plotTSeriesAbsolute(grand_accum, ref_grand_accum, ...
    scale_factor, field_name, use_mean, yrange, yname, ...
    subregion_handling, ref_comp_type, parent)

setup_disp_list = {'Control', 'Bottom', 'Top'};
setup_name_list = {'control', 'bot_shade', 'top_shade'};

ax_list = cell(3,1);

num_subplots = nnz(~structfun(@isempty, grand_accum));
cur_subplot = 1;

for setup_idx=1:3
    setup_name = setup_name_list{setup_idx};
    test_accum = grand_accum.(setup_name);
    
    if isempty(test_accum)
        continue;
    end
    
    ax = subplot(num_subplots, 1, cur_subplot, 'Parent', parent);
    ax_list{setup_idx} = ax;
    
    for ref_expr_idx=0:1
        if ref_expr_idx==0 && isempty(ref_grand_accum)
            continue;
        end        
        
        if ref_expr_idx==0
            switch subregion_handling
                case 'separate'
                    line_colors = {[1,0,1], [1,0.8,0]};
                    fill_colors = {[1,0,1], [1,0.8,0]};
                case {'combine', 'use_first'}
                    line_colors = {[0,0,0]};
                    fill_colors = {[0,0,0]};
                otherwise
                    error 'unknown subregion handling';
            end
            
            switch ref_comp_type
                case 'left_right'
                    local_accum = ref_grand_accum.(setup_name);
                case 'ref_control'
                    local_accum = ref_grand_accum.control;
                case 'ref_respective'         
                    local_accum = ref_grand_accum.(setup_name);
                otherwise
                    error 'unrecognized comp type';
            end
        else
            switch subregion_handling
                case 'separate'
                    line_colors = {[0,0,1], [1,0,0]};
                    fill_colors = {[0,0,1], [1,0,0]};
                case {'combine', 'use_first'}
                    line_colors = {[0,0,1]};
                    fill_colors = {[0,0,1]};
                otherwise
                    error 'unknown subregion handling';
            end
            
            local_accum = grand_accum.(setup_name);
        end
        
        fill_alpha = 0.2;
        
        if isempty(local_accum)
            continue;
        end
        
        time_base = local_accum(1).time / 60; % sec to minutes
        
        vals = zeros(size(local_accum,1), size(local_accum,2), length(time_base));
        total_time = zeros(size(local_accum,1), size(local_accum,2), length(time_base));
        
        for subr_idx=1:size(local_accum,2)
            for trial_idx=1:size(local_accum,1)
                vals(trial_idx,subr_idx,:) = local_accum(trial_idx,subr_idx).(field_name);
                total_time(trial_idx,subr_idx,:) = local_accum(trial_idx,subr_idx).total_time;
            end
        end
        
        vals = vals * scale_factor;
        
        switch subregion_handling
            case 'separate'
                % do nothing
            case 'combine'
                
                c_vals = zeros(size(vals,1), 1, size(vals,3));
                
                for trial_idx=1:size(vals,1)
                    c_vals(trial_idx,1,:) = combineLeftRight(...
                        vals(trial_idx,1,:), total_time(trial_idx,1,:), ...
                        vals(trial_idx,2,:), total_time(trial_idx,2,:));
                end
                
                vals = c_vals;
                
            case 'use_first'
                vals = vals(:, 1, :);
            otherwise
                error 'unrecognized value for subregion_handling';
        end
        
        if use_mean
            avg = mean(vals, 1, 'omitnan');
        else
            avg = median(vals, 1, 'omitnan');
        end
        
        num = sum(~isnan(vals), 1);
        sem = std(vals, 1, 'omitnan')./sqrt(num);
        
        avg = fillmissing(avg, 'linear');
        sem = fillmissing(sem, 'linear');
        
        for subr_idx=1:size(vals,2)
            avg_subr = squeeze(avg(1,subr_idx,:))';
            sem_subr = squeeze(sem(1,subr_idx,:))';
            
            fill_x = [time_base, time_base(end:-1:1)];
            fill_y = [avg_subr + sem_subr, avg_subr(end:-1:1) - sem_subr(end:-1:1)];
            fill_h = patch(fill_x, fill_y, [0, 0, 0], 'Parent', ax);
            hold(ax, 'on');
            
            fill_h.FaceAlpha = fill_alpha;
            fill_h.LineStyle = 'none';
            fill_h.FaceColor = fill_colors{subr_idx};
            
            line_h = plot(ax, time_base, avg_subr);
            line_h.LineWidth = 1.5;
            line_h.Color = line_colors{subr_idx};
            
            set(ax, 'YTick', yrange);
            
            hold(ax, 'off');
            
            if ~isempty(yrange)
                ylim(ax, yrange);
            end
            
            ylabel(ax, setup_disp_list{setup_idx});
        end
    end
    
    cur_subplot = cur_subplot + 1;
end

ax_occupied = ~cellfun(@isempty, ax_list);
first_ax = find(ax_occupied, 1, 'first');
last_ax = find(ax_occupied, 1, 'last');

xlabel(ax_list{last_ax}, 'Time (min)');
title(ax_list{first_ax}, yname);

end


function ax_list = plotTSeriesPreference(grand_accum, ref_grand_accum, ...
    field_name, use_mean, yrange, yname, norm_indiv, ref_comp_type, ...
    parent)

setup_disp_list = {'Control', 'Bottom', 'Top'};
setup_name_list = {'control', 'bot_shade', 'top_shade'};
ax_list = cell(3, 1);

num_subplots = nnz(~structfun(@isempty, grand_accum));
cur_subplot = 1;

for setup_idx=1:3
    setup_name = setup_name_list{setup_idx};
    test_accum = grand_accum.(setup_name);

    if isempty(test_accum)
        continue;
    end
        
    ax = subplot(num_subplots, 1, cur_subplot, 'Parent', parent);
    ax_list{setup_idx} = ax;

    for ref_expr_idx=0:1
        if ref_expr_idx==0 && isempty(ref_grand_accum)
            continue;
        end    
        
        if ref_expr_idx==0
            line_color = [0,0,0];
            fill_color = [0,0,0];
            
            switch ref_comp_type
                case 'left_right'
                    local_accum = ref_grand_accum.(setup_name);
                case 'ref_control'
                    local_accum = ref_grand_accum.control;
                case 'ref_respective'         
                    local_accum = ref_grand_accum.(setup_name);
                otherwise
                    error 'unrecognized comp type';
            end
        else
            line_color = [0,0,1];
            fill_color = [0,0,1];
            
            local_accum = grand_accum.(setup_name);
        end
        
        fill_alpha = 0.2;

        if isempty(local_accum)
            continue;
        end
        
        time_base = local_accum(1).time / 60; % sec to minutes
        
        if size(local_accum,2) ~= 2
            error 'invalid number of subregions';
        end
        
        left_vals = zeros(size(local_accum,1), length(time_base));
        for trial_idx=1:size(local_accum,1)
            left_vals(trial_idx,:) = local_accum(trial_idx,1).(field_name);
        end
        
        right_vals = zeros(size(local_accum,1), length(time_base));
        for trial_idx=1:size(local_accum,1)
            right_vals(trial_idx,:) = local_accum(trial_idx,2).(field_name);
        end
        
        weird_mask = xor(isnan(left_vals), isnan(right_vals));
        if nnz(weird_mask) > 0
            error 'found invalid data';
        end
        
        vals = (left_vals - right_vals) ./ (left_vals + right_vals);
        
        if norm_indiv
            indiv_sign = sign(mean(vals, 2, 'omitnan'));
            vals = bsxfun(@times, vals, indiv_sign);
        end
        
        if use_mean
            avg = mean(vals, 1, 'omitnan');
        else
            avg = median(vals, 1, 'omitnan');
        end
        
        num = sum(~isnan(vals), 1);
        sem = std(vals, 1, 'omitnan')./sqrt(num);
        
        avg = fillmissing(avg, 'linear');
        sem = fillmissing(sem, 'linear');
        
        fill_x = [time_base, time_base(end:-1:1)];
        fill_y = [avg + sem, avg(end:-1:1) - sem(end:-1:1)];
        fill_h = patch(fill_x, fill_y, fill_color, 'Parent', ax);
        hold(ax, 'on');
        
        fill_h.FaceAlpha = fill_alpha;
        fill_h.LineStyle = 'none';
        
        line_h = plot(ax, time_base, avg);
        line_h.LineWidth = 1.5;
        line_h.Color = line_color;
        
        line_b = plot(ax, [min(time_base), max(time_base)], [0, 0]);
        line_b.LineWidth = 1.0;
        line_b.Color = [0.5, 0.5, 0.5];
        
        set(ax, 'YTick', yrange);
        
        hold(ax, 'off');
        
        if ~isempty(yrange)
            ylim(ax, yrange);
        end
        
        ylabel(ax, setup_disp_list{setup_idx});
    end
    
    cur_subplot = cur_subplot + 1;
end

ax_occupied = ~cellfun(@isempty, ax_list);
first_ax = find(ax_occupied, 1, 'first');
last_ax = find(ax_occupied, 1, 'last');

xlabel(ax_list{last_ax}, 'Time (min)');
title(ax_list{first_ax}, yname);

end
