function params = getExperimentParams()
%This is a list of experimental parameters for each trial that is run in
%the anxiety-like behavior study. The first column is the date that the
%experiment took place. The second column is the trial number that restarts
%at the beginning of each day. The third column is the dpf of the larvae
%that are in the experiment, The fourth coulumn is the protocol name of the
%experiment. The fifth column is an array that contains the numbers of the
%wells that have bad tracking data. If there are no issues the array is
%left blank.


params = {...
%      '2018_03_26',1,5,'two_hour_movement';
%      '2018_03_27',1,6,'top_bottom';
%      '2018_03_27',2,6,'top_bottom';
%      '2018_03_27',3,6,'top_bottom';
%      '2018_03_27',4,6,'top_bottom';
%      '2018_03_27',5,6,'top_bottom';
%      '2018_03_27',6,6,'top_bottom';
%      '2018_03_27',7,6,'two_hour_movement';
%      '2018_03_28',1,7,'top_bottom';
%      '2018_03_28',2,7,'top_bottom';
%      '2018_03_28',3,7,'top_bottom';
%      '2018_04_02',1,5,'top_bottom';
%      '2018_04_02',2,5,'top_bottom';
%      '2018_04_02',3,5,'top_bottom';
%      '2018_04_02',4,5,'top_bottom';
%      '2018_04_02',5,5,'top_bottom';
%      '2018_04_02',6,5,'top_bottom';
%      '2018_04_09',1,5,'top_bottom';
%      '2018_04_09',2,5,'top_bottom';
%      '2018_04_09',3,5,'top_bottom';
%      '2018_04_09',4,5,'top_bottom';
%      '2018_04_09',5,5,'top_bottom';
%      '2018_04_09',6,5,'top_bottom';
%      '2018_04_10',1,6,'top_bottom';
%      '2018_04_10',2,6,'top_bottom';
%      '2018_04_10',3,6,'top_bottom';
%      '2018_04_10',4,6,'top_bottom';
%      '2018_04_11',1,7,'top_bottom';
%      '2018_04_11',2,7,'top_bottom';
%      '2018_04_11',3,7,'top_bottom';
%      '2018_04_17',1,4,'top_bottom';
%      '2018_04_17',2,4,'top_bottom';
%      '2018_04_17',3,4,'top_bottom';
%      '2018_04_17',4,4,'top_bottom';
%      '2018_04_17',5,4,'top_bottom';
%      '2018_04_17',6,4,'top_bottom';
     '2018_04_19',1,6,'no_feed',[];
     '2018_04_20',1,7,'no_feed',[3];
     '2018_04_20',2,7,'no_feed',[];
     '2018_04_20',3,7,'no_feed',[];
     '2018_04_20',4,7,'no_feed',[];
     '2018_04_20',5,7,'no_feed',[];
     '2018_04_23',1,6,'top_bottom',[4];
     '2018_04_23',2,6,'eshock_1volt',[];
     '2018_04_23',3,6,'eshock_6volt',[];
     '2018_04_23',4,6,'eshock_12volt',[];
     '2018_04_23',5,6,'eshock_1volt_3sec',[];
     '2018_04_24',1,6,'eshock_6volt_3sec',[];
     '2018_04_24',2,6,'eshock_12volt_3sec',[];
     '2018_04_24',3,6,'top_bottom',[];
     '2018_04_30',1,4,'top_bottom',[1, 3, 6];
     '2018_04_30',2,4,'top_bottom',[];
     '2018_04_30',3,4,'top_bottom',[];
     '2018_04_30',4,4,'top_bottom',[5];
     '2018_04_30',5,4,'top_bottom',[4];
     '2018_04_30',6,4,'top_bottom',[];
     '2018_05_01',1,6,'eshock_1volt',[];
     '2018_05_01',2,6,'eshock_1volt',[];
     '2018_05_01',3,6,'eshock_1volt',[];
     '2018_05_01',4,6,'eshock_1volt',[];
     '2018_05_01',5,6,'eshock_1volt',[];
     '2018_05_02',1,6,'no_feed',[];
     '2018_05_02',2,6,'no_feed',[];
     '2018_05_02',3,6,'no_feed',[];
     '2018_05_03',1,7,'no_feed',[3];
     '2018_05_03',2,7,'no_feed',[];
     '2018_05_03',3,7,'no_feed',[];
     '2018_05_03',4,7,'no_feed',[];
     '2018_05_03',5,7,'no_feed',[2];
     '2018_05_03',6,6,'eshock_1volt',[];
     '2018_05_04',1,7,'top_bottom',[];
     '2018_05_04',2,7,'top_bottom',[];
     '2018_05_04',3,7,'top_bottom',[];
     '2018_05_04',4,7,'top_bottom',[];
     '2018_05_04',5,7,'top_bottom',[5];
     '2018_05_04',6,7,'top_bottom',[];
     '2018_05_07',1,4,'eshock_1volt',[1, 3, 4];
     '2018_05_07',2,4,'eshock_1volt',[];
     '2018_05_07',3,4,'eshock_1volt',[1];
     '2018_05_09',1,6,'eshock_1volt_vtest',[];
     '2018_05_09',2,6,'eshock_3volt_vtest',[];
     '2018_05_09',3,6,'eshock_6volt_vtest',[];
     '2018_05_09',4,6,'eshock_9volt_vtest',[];
     '2018_05_09',5,6,'eshock_12volt_vtest',[];
     '2018_05_15',1,4,'size_75mm_bottom',[];
     '2018_05_15',2,4,'size_75mm_bottom',[];
     '2018_05_15',3,4,'size_75mm_bottom',[];
     '2018_05_17',1,7,'size_75mm_bottom',[];
     '2018_05_17',2,7,'size_75mm_bottom',[];
     '2018_05_17',3,7,'size_75mm_bottom',[];
     '2018_05_17',4,7,'size_75mm_bottom',[];
     '2018_05_18',1,7,'eshock_5sec',[];
     '2018_05_18',2,7,'eshock_1sec',[];
     '2018_05_18',3,7,'eshock_2sec',[];
     '2018_05_18',4,7,'eshock_3sec',[];
     '2018_05_18',5,7,'eshock_7sec',[];
     '2018_05_29',2,7,'eshock_2sec',[];
     '2018_05_29',3,7,'size_55mm_bottom',[];
     '2018_05_29',4,7,'size_55mm_bottom',[];
     '2018_05_29',5,7,'size_75mm_bottom',[];
     '2018_05_29',6,7,'size_75mm_bottom',[];
     '2018_05_29',7,7,'size_75mm_bottom',[];
     '2018_05_29',8,7,'size_75mm_control',[];
     '2018_05_31',1,7,'eshock_1sec',[];
     '2018_05_31',2,7,'eshock_3sec',[];
     '2018_05_31',3,7,'eshock_5sec',[];
     '2018_05_31',4,7,'size_55mm_bottom',[];
     '2018_05_31',5,7,'size_55mm_bottom',[1];
     '2018_05_31',6,7,'size_75mm_bottom',[];
     '2018_05_31',7,7,'size_75mm_bottom',[];
     '2018_05_31',8,7,'size_75mm_bottom',[];
     '2018_05_31',9,7,'size_75mm_bottom',[];
     '2018_05_31',10,7,'eshock_7sec',[];
     '2018_06_05',1,7,'size_55mm_bottom',[];
     '2018_06_05',2,7,'size_55mm_bottom',[];
     '2018_06_05',3,7,'size_75mm_bottom',[];
     '2018_06_05',4,7,'size_75mm_control',[];
     '2018_06_05',5,7,'size_75mm_control',[];
     '2018_06_05',6,7,'size_75mm_control',[];
     '2018_06_06',1,7,'no_feed',[3, 5];
     '2018_06_06',2,7,'no_feed',[4];
     '2018_06_06',3,7,'no_feed',[];
     '2018_06_06',4,7,'no_feed',[5];
     '2018_06_06',5,7,'no_feed',[];
     '2018_06_06',6,7,'no_feed',[];
     '2018_06_07',1,7,'size_55mm_control',[];
     '2018_06_07',2,7,'size_55mm_control',[];
     '2018_06_07',3,7,'size_75mm_control',[];
     '2018_06_07',4,7,'size_75mm_control',[];
     '2018_06_07',5,7,'size_75mm_control',[];
     '2018_06_12',1,7,'size_55mm_control',[];
     '2018_06_12',2,7,'size_55mm_control',[];
     '2018_06_12',3,7,'size_55mm_control',[];
     '2018_06_12',4,7,'size_55mm_control',[];
     '2018_06_13',1,7,'no_feed',[];
     '2018_06_19',1,7,'size_75mm_control',[];
     '2018_06_19',2,7,'size_75mm_control',[];
     '2018_06_19',3,7,'size_75mm_control',[];
     '2018_06_29',1,7,'size_75mm_control',[];
     %'2018_06_29',2,7,'size_75mm_control',[]; see notebook 1/23/19
     '2018_07_05',1,7,'no_feed',[];
     '2018_07_05',2,7,'no_feed',[];
     '2018_07_05',3,7,'no_feed',[];
     '2018_07_05',4,7,'no_feed',[];
     '2018_07_05',5,7,'no_feed',[3];
     '2018_07_05',6,7,'no_feed',[];
     '2018_07_19',1,7,'eshock_3ma',[6];
     '2018_07_19',3,7,'size_75mm_control',[];
     '2018_07_19',4,7,'size_75mm_bottom',[];
     '2018_07_19',5,7,'size_55mm_bottom',[];
     '2018_07_19',6,7,'size_55mm_bottom',[];
     '2018_07_24',1,7,'eshock_3ma',[5];
     '2018_07_24',2,7,'eshock_3ma',[];
     '2018_07_24',3,7,'eshock_3ma',[];
     '2018_07_24',4,7,'eshock_3ma',[];
     '2018_07_25',1,6,'top_bottom',[3, 4];
     '2018_07_25',2,6,'top_bottom',[3];
     '2018_07_25',3,6,'top_bottom',[4];
     '2018_07_25',4,6,'top_bottom',[];
     '2018_07_25',5,7,'top_bottom',[];
     '2018_07_25',6,7,'top_bottom',[];
     '2018_07_25',7,7,'top_bottom',[];
     '2018_07_25',8,7,'top_bottom',[];
     '2018_07_31',1,4,'top_bottom',[5];
     '2018_07_31',2,4,'top_bottom',[2];
     '2018_07_31',3,4,'top_bottom',[];
     '2018_07_31',4,4,'top_bottom',[2];
     '2018_07_31',5,4,'top_bottom',[];
     '2018_07_31',6,4,'top_bottom',[];
     '2018_07_31',7,4,'top_bottom',[];
     '2018_07_31',8,5,'top_bottom',[];
     '2018_07_31',9,5,'top_bottom',[];
     '2018_07_31',10,5,'top_bottom',[];
     '2018_07_31',11,5,'top_bottom',[];
     '2018_08_01',1,5,'top_bottom',[];
     '2018_08_01',2,5,'top_bottom',[];
     '2018_08_01',3,5,'top_bottom',[];
     '2018_08_01',4,5,'top_bottom',[1];
     '2018_08_01',5,5,'top_bottom',[];
     '2018_08_01',6,5,'top_bottom',[5];
     '2018_08_01',7,5,'top_bottom',[2];
     '2018_08_01',8,6,'top_bottom',[];
     '2018_08_02',1,7,'top_bottom',[];
     '2018_08_02',2,7,'top_bottom',[];
     '2018_08_02',3,7,'top_bottom',[];
     '2018_08_06',1,5,'top_bottom',[];
     '2018_08_06',2,5,'top_bottom',[];
     '2018_08_06',3,6,'top_bottom',[];
     '2018_08_06',4,6,'top_bottom',[];
     '2018_08_07',1,6,'top_bottom',[5];
     '2018_08_07',2,6,'top_bottom',[];
     '2018_08_07',3,6,'top_bottom',[];
     '2018_08_07',4,6,'top_bottom',[];
     '2018_08_07',5,6,'top_bottom',[5];
     '2018_08_09',1,7,'eshock_3ma',[];
     '2018_08_09',2,7,'eshock_3ma',[];
     '2018_08_09',3,7,'eshock_3ma',[];
     '2018_08_09',4,7,'eshock_3ma',[6];
    %'2018_08_09',5,7,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_08_09',6,7,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_08_10',1,7,'eshock_3ma',[];  see notebook 10/22/2018
     '2018_08_10',2,7,'eshock_3ma',[4];
     '2018_08_10',3,7,'eshock_3ma',[];
     '2018_08_13',1,5,'eshock_3ma',[4];
     '2018_08_13',2,5,'eshock_3ma',[];
     '2018_08_13',3,5,'eshock_3ma',[];
    %'2018_08_15',1,5,'eshock_3ma',[]; see notebook 1/16/2019
     '2018_08_17',1,7,'size_55mm_control',[];
     '2018_08_17',2,7,'size_55mm_control',[];
     '2018_08_17',3,7,'size_55mm_top',[];
     '2018_08_17',4,7,'size_55mm_top',[];
     '2018_08_17',5,7,'size_55mm_top',[2];
     '2018_08_17',6,7,'size_55mm_top',[];
     '2018_08_17',7,7,'size_55mm_top',[];
     '2018_08_17',8,7,'size_55mm_top',[];
     '2018_08_20',1,7,'size_55mm_top',[];
     '2018_08_20',2,7,'size_55mm_top',[];
     '2018_08_20',3,7,'size_75mm_control',[];
     '2018_08_20',4,7,'size_75mm_control',[];
     '2018_08_20',5,7,'size_75mm_control',[];
     '2018_08_20',6,7,'size_75mm_bottom',[];
     '2018_08_20',7,7,'size_75mm_bottom',[];
     '2018_08_21',1,7,'size_75mm_bottom',[];
     '2018_08_22',1,7,'size_75mm_top',[];
     '2018_08_22',2,7,'size_75mm_top',[];
     '2018_08_22',3,7,'size_75mm_top',[];
     '2018_08_22',4,7,'size_75mm_top',[];
     '2018_08_23',1,5,'eshock_3ma',[];
     '2018_08_23',2,5,'eshock_3ma',[];
     '2018_08_23',3,5,'eshock_3ma',[];
     '2018_08_23',4,5,'eshock_3ma',[];
     '2018_08_24',1,5,'eshock_3ma',[];
    %'2018_08_24',2,5,'eshock_3ma',[]; see notebook 1/16/2019
     '2018_08_28',1,7,'size_75mm_top',[];
     '2018_08_28',2,7,'size_75mm_top',[];
     '2018_08_28',3,7,'size_75mm_top',[];
     '2018_08_28',4,7,'size_75mm_top',[];
     '2018_08_28',5,7,'size_75mm_top',[];
     '2018_08_28',6,7,'size_75mm_top',[];
     '2018_08_28',7,7,'size_75mm_top',[];
     '2018_08_29',1,7,'size_75mm_top',[];
     '2018_08_29',2,7,'size_75mm_top',[];
     '2018_08_29',3,7,'size_75mm_top',[];
     '2018_08_29',4,7,'size_75mm_top',[];
     '2018_08_29',5,7,'size_75mm_top',[];
    %'2018_09_05',1,7,'top_bottom_AITC',[]; see notebook 9/5/2018
     '2018_09_05',2,7,'top_bottom_AITC_white',[5];
     '2018_09_06',1,7,'top_bottom_AITC_white',[];
     '2018_09_11',1,7,'top_bottom_AITC_white',[];
     '2018_09_11',2,7,'top_bottom_AITC_white',[];
    %'2018_09_11',3,7,'top_bottom_AITC',[]; see notebook 9/11/2018
     '2018_09_12',1,7,'top_bottom_AITC_white',[];
    %'2018_09_12',2,7,'top_bottom_AITC',[]; see notebook 9/12/2018
    %'2018_09_12',3,7,'top_bottom_AITC',[]; see notebook 9/12/2018
    %'2018_09_14',1,7,'top_bottom_AITC',[]; see notebook 9/14/2018
    %'2018_09_18',1,7,'top_bottom_AITC',[]; see notebook 9/18/2018
    %'2018_09_18',2,7,'top_bottom_AITC',[]; see notebook 9/18/2018
     '2018_09_19',1,7,'top_bottom_AITC_white',[];
     '2018_09_19',2,7,'top_bottom_AITC_white',[];
    %'2018_10_01',1,5,'eshock_3ma',[]; see notebook 1/16/2019
     '2018_10_01',2,5,'eshock_3ma',[5];
    %'2018_10_03',1,5,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_10_03',2,5,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_10_09',1,5,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_10_09',2,5,'eshock_3ma',[]; see notebook 1/16/2019
    %'2018_10_09',3,5,'eshock_3ma',[]; see notebook 1/16/2019
     '2018_10_10',1,5,'eshock_3ma',[];
     '2018_10_10',2,5,'eshock_3ma',[];
     '2018_10_15',1,5,'eshock_6ma',[];
     '2018_10_15',2,5,'eshock_6ma',[];
     '2018_10_17',1,5,'eshock_6ma',[];
     '2018_10_22',1,5,'eshock_6ma',[];
     '2018_10_22',2,5,'eshock_6ma',[];
     '2018_10_22',3,5,'eshock_6ma',[5];
     '2018_10_22',4,5,'eshock_6ma',[];
     '2018_10_23',1,7,'top_bottom_AITC_black',[];
     '2018_10_23',2,7,'top_bottom_AITC_black',[];
     '2018_10_23',3,7,'top_bottom_AITC_black',[];
     '2018_10_23',4,7,'top_bottom_AITC_black',[];
     '2018_10_23',5,7,'top_bottom_AITC_black',[];
     '2018_10_23',6,7,'top_bottom_AITC_black',[];
     '2018_10_29',1,5,'eshock_6ma',[];
     '2018_10_29',2,5,'eshock_6ma',[];
     '2018_10_29',3,5,'eshock_6ma',[1];
     '2018_10_31',1,7,'eshock_6ma',[];
     '2018_10_31',2,7,'eshock_6ma',[];
     '2018_10_31',3,7,'eshock_6ma',[];
     '2018_11_05',1,5,'eshock_6ma',[];
     '2018_11_05',2,5,'eshock_6ma',[];
     '2018_11_07',1,7,'eshock_6ma',[];
     '2018_11_19',1,5,'eshock_6ma',[4, 6];
     '2018_11_20',1,7,'eshock_6ma',[];
     '2018_11_20',2,7,'eshock_6ma',[];
     '2018_11_21',1,7,'eshock_6ma',[];
     '2018_11_21',2,7,'eshock_6ma',[];
     '2018_11_26',1,7,'eshock_6ma',[];
     '2018_11_26',2,7,'eshock_6ma',[];
     '2018_11_26',3,7,'eshock_6ma',[];
     '2018_12_06',1,7,'eshock_6ma',[];
     '2018_12_06',2,7,'eshock_6ma',[];
     '2018_12_06',3,7,'eshock_6ma',[];
     %'2018_12_06',4,7,'eshock_6ma',[]; See notebook 7/26/19
     '2019_01_30',1,7,'bottom_AITC',[];
     '2019_01_30',2,7,'bottom_AITC',[];
     '2019_01_30',3,7,'bottom_AITC',[];
     '2019_01_30',4,7,'bottom_AITC',[];
     '2019_01_31',1,7,'bottom_proj_overhead',[];
     '2019_01_31',2,7,'bottom_proj_overhead',[];
     '2019_01_31',3,7,'bottom_proj',[];
     '2019_01_31',4,7,'bottom_proj',[1];%See notebook 7/26/19
     '2019_01_31',5,7,'bottom_proj',[];
     '2019_02_04',1,4,'celatum_prelim0_e3',[];% did not curate any of the celatum trials
     '2019_02_04',2,4,'celatum_prelim0_media',[];
     '2019_02_04',3,4,'celatum_prelim0_exper',[];
     '2019_02_04',4,4,'celatum_prelim0_e3',[];
     '2019_02_04',5,4,'celatum_prelim0_media',[];
     '2019_02_04',6,4,'celatum_prelim0_exper',[];
     '2019_02_05',1,5,'celatum_prelim0_e3',[];
     '2019_02_05',2,5,'celatum_prelim0_media',[];
     '2019_02_05',3,5,'celatum_prelim0_exper',[];
     '2019_02_05',4,5,'celatum_prelim0_e3',[];
     '2019_02_05',5,5,'celatum_prelim0_media',[];
     '2019_02_05',6,5,'celatum_prelim0_exper',[];
     '2019_02_06',1,7,'celatum_prelim1_e3',[];
     '2019_02_06',2,7,'celatum_prelim1_media',[];
     '2019_02_06',3,7,'celatum_prelim1_exper',[];
     '2019_02_06',4,7,'celatum_prelim1_exper',[];
     '2019_02_06',5,7,'celatum_prelim1_media',[];
     '2019_02_06',6,7,'celatum_prelim1_e3',[];
     '2019_02_12',1,7,'bottom_AITC',[];
     '2019_02_13',1,7,'control_AITC',[];
     '2019_02_13',2,7,'control_AITC',[];
     '2019_02_13',3,7,'control_AITC',[2];%See notebook 7/26/19
     '2019_02_13',4,7,'control_AITC',[];
     '2019_02_19',1,7,'top_AITC',[2, 3];%See notebook 7/26/19
     '2019_02_19',2,7,'top_AITC',[];
     '2019_02_19',3,7,'top_AITC',[];
     '2019_02_19',4,7,'top_AITC',[];
     '2019_02_19',5,7,'top_AITC',[5];%See notebook 10/9/19
     '2019_02_19',6,7,'top_AITC',[];
     '2019_02_26',1,7,'flash',[];
     '2019_02_27',1,7,'flash',[];
     '2019_02_27',2,7,'flash',[];
     '2019_02_27',3,7,'flash',[];
     '2019_02_28',1,7,'flash',[5];%See notebook 7/26/19
     '2019_02_28',2,7,'flash',[];
     '2019_03_19',1,7,'flash_20sec',[];
     '2019_03_21',1,7,'flash_20sec',[];
     '2019_03_21',2,7,'flash_20sec',[];
     '2019_04_01',1,7,'flash_20sec',[];
     '2019_04_01',2,7,'flash_20sec',[];
     '2019_04_02',1,7,'flash_20sec',[];
     %'2019_04_02',2,7,'flash_20sec',[]; See notebook 7/29/19
     '2019_04_02',3,7,'flash_20sec',[];
     '2019_04_02',4,7,'flash_20sec',[];
     '2019_04_04',1,7,'flash_20sec_hb',[];
     '2019_04_04',2,7,'flash_20sec_hb',[];
     '2019_04_04',3,7,'flash_20sec_hb',[];
     '2019_04_04',4,7,'flash_20sec_hb',[];
     '2019_04_12',1,7,'flash_20sec_hb',[];
     '2019_04_12',2,7,'flash_20sec_hb',[];
     '2019_04_12',3,7,'flash_20sec_hb',[];
     '2019_04_18',1,7,'flash_30sec_4min',[];
     '2019_04_18',2,7,'flash_30sec_4min',[];
     '2019_04_18',3,7,'flash_30sec_4min',[];
     '2019_04_18',4,7,'flash_30sec_4min',[];
     '2019_04_18',5,7,'flash_30sec_4min',[];
     '2019_04_19',1,7,'flash_30sec_4min',[];
     '2019_04_25',1,7,'flash_30sec_4min',[];
     '2019_04_25',2,7,'flash_30sec_4min',[2];% see notebook 7/29/19
     '2019_04_25',3,7,'flash_30sec_4min',[6];% see notebook 7/29/19
     '2019_04_30',1,7,'flash_2min_2min',[];
     '2019_04_30',2,7,'flash_2min_2min',[];
     '2019_05_01',1,7,'flash_2min_2min',[];
     '2019_05_01',2,7,'flash_2min_2min',[];
     '2019_05_01',3,7,'flash_2min_2min',[];
     '2019_05_01',4,7,'flash_2min_2min',[];
%      '2019_05_07',1,7,'flash_2min_8min',[3, 6]; see notebook 7/29/19
%      '2019_05_07',2,7,'flash_2min_8min',[];     for all deletions
%      '2019_05_08',1,7,'flash_2min_8min',[];     up until 2019_05_14
%      '2019_05_08',2,7,'flash_2min_8min',[];
%      '2019_05_09',1,7,'flash_2min_8min',[];
%      '2019_05_09',2,7,'flash_2min_8min',[];
%      '2019_05_14',1,7,'flash_2min_8min',[];
     '2019_05_23',1,7,'flash_2min_8min',[3]; %see notebook 7/29/19
     '2019_05_23',2,7,'flash_2min_8min',[4]; %see notebook 7/29/19
     '2019_05_24',1,7,'flash_2min_8min',[];
     '2019_05_29',1,7,'flash_2min_8min_lf',[4]; %see notebook 7/29/19
     '2019_05_29',2,7,'flash_2min_8min_lf',[];
     '2019_05_30',1,7,'flash_2min_8min_lf',[];
     '2019_05_30',2,7,'bottom_proj_v2',[2]; %see notebook 7/30/19
     '2019_05_30',3,7,'bottom_proj_v2',[];
     '2019_05_30',4,7,'bottom_proj_v2',[];
     '2019_05_30',5,7,'bottom_proj_v2',[1,2,4,6]; %see notebook 7/30/19
     '2019_05_30',6,7,'bottom_proj_v2',[1,2,3,4,6]; %see notebook 7/30/19
     '2019_06_04',1,7,'flash_2min_8min_rb',[];
     '2019_06_04',2,7,'flash_2min_8min_rb',[];
     '2019_06_04',3,7,'flash_2min_8min_rb',[3]; %see notebook 7/30/19
     '2019_06_05',1,7,'flash_2min_8min_rb',[3,4]; %see notebook 7/31/19
     '2019_06_05',2,7,'flash_2min_8min_rb',[3,5]; %see notebook 7/31/19
     '2019_06_06',1,7,'bottom_proj_rb',[6]; %see notebook 7/31/19
     '2019_06_06',2,7,'bottom_proj_rb',[5]; %see notebook 7/31/19
     '2019_06_06',3,7,'bottom_proj_rb',[4,6]; %see notebook 7/31/19
     '2019_06_07',1,7,'bottom_proj_rb',[];
     '2019_06_07',2,7,'bottom_proj_rb',[];
     '2019_06_07',3,7,'bottom_proj_rb',[];
     '2019_06_07',4,7,'bottom_proj_rb',[2]; %see notebook 7/31/19
     '2019_06_26',1,7,'AITC_rb',[2,3]; %see notebook 7/31/19
     '2019_06_26',2,7,'AITC_rb',[3]; %see notebook 7/31/19
     '2019_06_26',3,7,'AITC_rb',[1,3]; %see notebook 7/31/19
     '2019_06_26',4,7,'AITC_rb',[4]; %see notebook 7/31/19
     '2019_06_28',1,7,'AITC_meto',[];%Needs Curation
     '2019_06_28',2,7,'AITC_meto',[];%Needs Curation
     '2019_06_28',3,7,'AITC_meto',[];%Needs Curation
     '2019_07_17',1,7,'AITC_rb',[1,3];%see notebook 10/9/19
     '2019_07_17',2,7,'AITC_rb',[1,3,4];%see notebook 10/9/19
     '2019_07_17',3,7,'AITC_rb',[3];%see notebook 10/9/19
     '2019_07_17',4,7,'AITC_rb',[1,3];%see notebook 10/9/19
     '2019_07_17',5,7,'AITC_rb',[1,2,3];%see notebook 10/9/19
     '2019_07_19',1,7,'AITC_meto',[];%Needs Curation
     '2019_07_19',2,7,'AITC_meto',[];%Needs Curation
     '2019_07_26',1,7,'AITC_meto',[];%Needs Curation
     '2019_07_26',2,7,'AITC_meto',[4];%Needs Curation
     '2019_07_31',1,7,'AITC_pb',[];%Needs Curation
     '2019_08_01',1,7,'AITC_pb',[];%Needs Curation
     '2019_08_01',2,7,'AITC_pb',[];%Needs Curation
     '2019_08_02',1,7,'AITC_pb',[];%Needs Curation
     '2019_08_02',2,7,'AITC_pb',[];%Needs Curation
     '2019_08_02',3,7,'AITC_pb',[];%Needs Curation
     '2019_08_06',1,7,'AITC_drugs',[];%Needs Curation
     '2019_08_06',2,7,'AITC_drugs',[2];% see notebook 8/8/19
     '2019_08_06',3,7,'AITC_drugs',[4];% see notebook 8/8/19
     '2019_08_06',4,7,'AITC_drugs',[4];% see notebook 8/8/19
     '2019_08_08',1,7,'AITC_drugs',[];%Needs Curation
     '2019_08_08',2,7,'AITC_drugs',[];%Needs Curation
     '2019_08_08',3,7,'AITC_drugs',[];%Needs Curation
     '2019_08_08',4,7,'AITC_drugs',[];%Needs Curation
     '2019_08_09',1,7,'AITC_drugs',[];%Needs Curation
     '2019_08_09',2,7,'AITC_drugs',[];%Needs Curation
     '2019_08_09',3,7,'AITC_drugs',[];%Needs Curation
     '2019_08_09',4,7,'AITC_drugs',[];%Needs Curation
     '2019_08_13',1,7,'AITC_drugs2',[];
     '2019_08_15',1,7,'AITC_drugs2',[3,5];%see notebook 10/7/19
     '2019_08_15',2,7,'AITC_drugs2',[1,2];%see notebook 10/7/19
     '2019_08_15',3,7,'AITC_drugs2',[3,6];%see notebook 10/7/19 & 10/9/19
     '2019_08_15',4,7,'AITC_drugs2',[5,6];%see notebook 10/7/19 & 10/9/19
     '2019_08_16',1,7,'AITC_drugs2',[];
     '2019_08_16',2,7,'AITC_drugs2',[];
     '2019_08_21',1,7,'AITC_drugs2',[];
     '2019_08_21',2,7,'AITC_drugs2',[3,5,6];%see notebook 10/7/19 & 10/9/19
     '2019_08_22',1,7,'AITC_drugs2',[];
     '2019_08_22',2,7,'AITC_drugs2',[];
     '2019_08_22',3,7,'AITC_drugs2',[2,4];%see notebook 10/9/19
     '2019_08_22',4,7,'AITC_drugs2',[2,5,6];%see notebook 10/8/19 & 10/9/19
     '2019_08_23',1,7,'AITC_drugs2',[];
     '2019_08_23',2,7,'AITC_drugs2',[1,6];%see notebook 10/8/19
     %'2019_08_23',3,7,'AITC_drugs2',[];see notebook 10/8/19
     %'2019_08_23',4,7,'AITC_drugs2',[];see notebook 10/8/19
     };
end


