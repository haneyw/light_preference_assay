function makeFakeDataPacing(mask_fname, traj_fname, vel, wall_dist, right_frac)
% This function makes a fake dataset that can be used to check that
% other analysis code is working correctly. It reads in the well
% information from mask_fname and outputs a trajectory file into
% traj_fname. The trajectory is a hemi-circular trajectory with velocity
% 'vel', wall distance 'wall_dist', and fraction of total time
% in right subregion of 'right_frac'. The arguments vel and wall_dist
% should be [Nx2] arrays where N is the number of masks and 2 is the 
% number of subregions. The argument right_frac should be a [Nx1] array
% where N is the number of masks.
% 
% For example:
% makeFakeDataPacing('trial0001_mask.mat', 'fake_traj.mat', ...
%    [100, 200; 100, 200; 20, 40; 20, 40; 200, 400; 200, 400], ...
%    [15, 30; 15, 30; 20, 40; 20, 40; 30, 60; 30, 60], ...
%    [0.4, 0.4, 0.6, 0.6, 0.8, 0.8]);
%
% Then manually check output of analyzeTopBottom

outlier_area = 6;
num_time_points = 10000;
num_bad_points = 100;
time_step = 1/30.0;

load(mask_fname, 'masks');
num_masks = numel(masks);

time = (0:(num_time_points-1))*time_step;
time = time';

raw_pos = zeros(2, num_masks, num_time_points);
xcorr_signal = ones(num_masks, num_time_points);

for area_idx=1:num_masks
    well_radius = masks(area_idx).params.radius;
    well_center = masks(area_idx).params.pos;
    
    right_trans = num_time_points * (1-right_frac(area_idx)) * time_step;
    
    % calculate trajectory for left hand side
    time_left = time(time <= right_trans);

    traj_radius = well_radius - wall_dist(area_idx, 1);
    omega = vel(area_idx, 1) / traj_radius;
    period = (2 * pi) / omega;
    
    time_rel = (time_left - period * floor(time_left / period))/period;
    
    theta_left = zeros(1, numel(time_left));
    theta_left(time_rel <= 0.5) = 2*pi * time_rel(time_rel<=0.5);
    theta_left(time_rel > 0.5) = pi - 2*pi*(time_rel(time_rel>0.5) - 0.5);
    theta_left = theta_left + pi/2;    
    
    x_left = well_center(1) + traj_radius .* cos(theta_left);
    y_left = well_center(2) + traj_radius .* sin(theta_left);
    
    % calculate trajectory for right hand side
    time_right = time(time > right_trans) - right_trans;

    traj_radius = well_radius - wall_dist(area_idx, 2);
    omega = vel(area_idx, 2) / traj_radius;
    period = (2 * pi) / omega;
    
    time_rel = (time_right - period * floor(time_right / period))/period;
    
    theta_right = zeros(1, numel(time_right));
    theta_right(time_rel <= 0.5) = 2*pi * time_rel(time_rel<=0.5);
    theta_right(time_rel > 0.5) = pi - 2*pi*(time_rel(time_rel>0.5) - 0.5);
    theta_right = theta_right + pi + pi/2;  
    
    x_right = well_center(1) + traj_radius .* cos(theta_right);
    y_right = well_center(2) + traj_radius .* sin(theta_right);
    
    % merge left and right
    theta = [theta_left, theta_right];
    x = [x_left, x_right];
    y = [y_left, y_right];
    
    heading = theta*180/pi + 90;
    
    selected_bad_points = unique(randi(num_time_points, [num_bad_points,1]));
    x(selected_bad_points) = 1e20;
    y(selected_bad_points) = 1e20;
    heading(selected_bad_points) = 1e20;
    
    raw_pos(1,area_idx,:) = x;
    raw_pos(2,area_idx,:) = y;
    raw_heading(area_idx,:) = heading;
    xcorr_signal(area_idx, selected_bad_points) = 0.0;
end

raw_pos(1:2,outlier_area,:) = 1e20;
raw_heading(area_idx,:) = 1e20;

save(traj_fname, 'time','raw_pos', 'raw_heading', 'xcorr_signal'); 

end