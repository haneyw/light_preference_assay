function grand_accum = aggregateTrials(ages, stage_list, desired_protocols, window_method)
%   The function accepts the following inputs
%       ages - [1x2] array where first element is lower age
%           and second element is upper age
%       stage_list - array of stages to examine. parameter is
%           ignored if protocol only has one stage.
%       field_name - name of field from lpg_analyzeLarvaTrajectories
%           to use for generating plots
%       desired_protocols - name of protocols to be analyzed,
%           can be either a string (e.g., 'top_bottom') or a 
%           cell array of protocols to be included 
%           (e.g., {'size_55mm_control', 'size_55mm_bottom'})
%       window_method - structure with field 'type', 'period', and
%           'times'. See lpg_analyzeLarvaTrajectoriesTimeDiv
%           for definitions of these fields.
%
%  It scans through all of the experimental data and produces
%  an ouput structure that contains all of the trials. The output
%  is a 1x1 struct with 3 fields: control, bot_shade, and top_shade.
%  Each of those fields contains an NxM struct array where N is the
%  number of trials and M is the number of subregions, with fields
%  corresponding to the output of lpg_analyzeLarvaTrajectoriesTimeDiv.


exper_params = getExperimentParams();

% the following array contains the data for all of the experiments.
grand_accum.control = [];
grand_accum.bot_shade = [];
grand_accum.top_shade = [];


for setup_idx=1:size(exper_params,1)
    date_name = exper_params{setup_idx,1};
    trial_num = exper_params{setup_idx,2};
    animal_age = exper_params{setup_idx,3};
    protocol_name = exper_params{setup_idx,4};
    outlier_areas = exper_params{setup_idx,5};
    
    if ~(nnz(strcmp(protocol_name, desired_protocols)) && ...
        	animal_age >= ages(1) && animal_age <= ages(2))
        continue;
    end
    
    props = getPlateProps(protocol_name);
    
    analysis_folder = [getDataFolder(), filesep, ...
        'analysis', filesep, date_name];
    
    kine_accum = [];
    time_base_accum = [];
    
    for stage_idx=1:length(stage_list)
        cur_stage = stage_list(stage_idx);
        
        if props.num_stages == 1
            if cur_stage ~= 1
                error 'invalid stage_idx value';
            end

            traj_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_traj.mat', trial_num)];

            mask_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_mask.mat', trial_num)];

            if ~isempty(props.subregions_type)
                subr_fname = [analysis_folder, filesep, ...
                    sprintf('trial%04d_subr.mat', trial_num)];
            else
                subr_fname = [];
            end

            heartb_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_heart_beat.mat', trial_num)];
        else
            traj_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_traj.mat', trial_num, cur_stage)];

            mask_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_mask.mat', trial_num, cur_stage)];

            if ~isempty(props.subregions_type)
                subr_fname = [analysis_folder, filesep, ...
                    sprintf('trial%04d_stage%02d_subr.mat', trial_num, cur_stage)];
            else
                subr_fname = [];
            end

            heartb_fname = [analysis_folder, filesep, ...
                sprintf('trial%04d_stage%02d_heart_beat.mat', trial_num, cur_stage)];
        end

        analyze_cfg.cutoff_freq = 5;
        analyze_cfg.min_time = 5;
        analyze_cfg.min_stretch = 15;
        
        % these parameters should be matched to playTrajectoryMovie values
        analyze_cfg.snr_high = 0.5;
        analyze_cfg.snr_med = 0.4;
        analyze_cfg.allow_dist_error = 10; % pixels
        
        analyze_cfg.switch_hysteresis = 2;
        analyze_cfg.window_type = window_method.type;
        analyze_cfg.window_period = window_method.period;
        analyze_cfg.window_times = window_method.times;
        analyze_cfg.partial_window_thresh = 0.9;
        analyze_cfg.allowed_missing_fraction = 0.05;
                
        % adjust window start time for stage start time
        if ~isempty(analyze_cfg.window_times)
            analyze_cfg.window_times(:,1) = analyze_cfg.window_times(:,1) - ...
                props.stage_start_time(cur_stage);
        end
        
        if ~strcmp(props.heartbeat_type{cur_stage}, 'none')
            load(heartb_fname, 'heartbeat');
            analyze_cfg.heartbeat_type = props.heartbeat_type{cur_stage};
            analyze_cfg.heartbeat_expect = props.heartbeat_expect{cur_stage};
            analyze_cfg.heartbeat_vals = heartbeat;  
        else
            analyze_cfg.heartbeat_type = 'none';
            analyze_cfg.heartbeat_expect = [];
            analyze_cfg.heartbeat_vals = [];
        end

        [kine,time_base] = lpg_analyzeLarvaTrajectoriesTimeDiv(...
            traj_fname, mask_fname, subr_fname, outlier_areas, analyze_cfg);
        
        time_base = time_base + props.stage_start_time(cur_stage);
        
        fnames = fieldnames(kine);
        
        if isempty(kine_accum)
            % if accumulator is empty, instantiate with empty arrays
            for field_idx=1:length(fnames)
                kine_accum.(fnames{field_idx}) = [];
            end
            
            kine_accum(size(kine,1), size(kine,2)).(fnames{1}) = [];
        end
        
        for elem_idx=1:numel(kine)
            for field_idx=1:length(fnames)
                kine_accum(elem_idx).(fnames{field_idx}) = ...
                    [kine_accum(elem_idx).(fnames{field_idx}), ...
                    kine(elem_idx).(fnames{field_idx})];
            end
        end
        
        time_base_accum = [time_base_accum, time_base];
    end
    
    kine = kine_accum;
    time_base = time_base_accum;
    clear kine_accum time_base_accum;
    
    switch protocol_name
        case {'top_bottom', 'no_feed', 'eshock_3ma', 'eshock_6ma',...
                'top_bottom_AITC_white','top_bottom_AITC_black',...
                'AITC_drugs', 'AITC_drugs2'}
            % add first and second wells to control group  
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 1);
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 2);
 
            % add third and fourth wells to bottom group
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 3);
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 4);
            
            % add fifth and sixth well to top group
            grand_accum = addToAccumArray(grand_accum, ...
                'top_shade', time_base, kine, 5);
            grand_accum = addToAccumArray(grand_accum, ...
                'top_shade', time_base, kine, 6);
        case 'size_55mm_control'
            % add wells to control group
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 1);
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 2);
        case 'size_55mm_bottom'
            % add wells to bottom group
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 1);
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 2);
        case 'size_55mm_top'
            % add wells to top group
            grand_accum = addToAccumArray(grand_accum, ...
                'top_shade', time_base, kine, 1);
            grand_accum = addToAccumArray(grand_accum, ...
                'top_shade', time_base, kine, 2);
        case 'size_75mm_control'
            % add well to control group
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 1);
        case 'size_75mm_bottom'
            % add well to bottom group
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 1);
        case 'size_75mm_top'
            % add well to top group
            grand_accum = addToAccumArray(grand_accum, ...
                'top_shade', time_base, kine, 1);
        case {'bottom_AITC', 'bottom_proj', 'bottom_proj_v2'...
                'bottom_proj_overhead','flash', 'flash_20sec',...
                'flash_20sec_hb','flash_30sec_4min'...
                'flash_2min_2min', 'flash_2min_8min', 'flash_2min_8min_lf'...
                'flash_2min_8min_rb', 'bottom_proj_rb', 'AITC_rb'...
                'AITC_meto'}
            for row_idx=1:6
                grand_accum = addToAccumArray(grand_accum, ...
                    'bot_shade', time_base, kine, row_idx);
            end
        case  {'celatum_prelim0_e3', 'celatum_prelim0_media', ...
                'celatum_prelim0_exper', 'celatum_prelim1_e3', ...
                'celatum_prelim1_media', 'celatum_prelim1_exper'}
            for row_idx=1:6
                grand_accum = addToAccumArray(grand_accum, ...
                    'bot_shade', time_base, kine, row_idx);
            end
        case {'control_AITC'}
            for row_idx=1:6
                grand_accum = addToAccumArray(grand_accum, ...
                    'control', time_base, kine, row_idx);
            end
        case {'top_AITC'}
            for row_idx=1:6
                grand_accum = addToAccumArray(grand_accum, ...
                    'top_shade', time_base, kine, row_idx);
            end
            
        case {'AITC_pb'}
            % add first, third and fifth wells to control group
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 1);
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 3);
            grand_accum = addToAccumArray(grand_accum, ...
                'control', time_base, kine, 5);
            
            % add second, fourth and sixth wells to bottom group
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 2);
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 4);
            grand_accum = addToAccumArray(grand_accum, ...
                'bot_shade', time_base, kine, 6);
             
        otherwise
            error 'unknown protocol';
    end
    
end

% trim the time base to the minimum 
min_time_idx = Inf;
accum_field_list = fieldnames(grand_accum);
for field_idx=1:length(accum_field_list)
    field_name = accum_field_list{field_idx};
    local_accum = grand_accum.(field_name);
    
    for trial_idx=1:size(local_accum,1)
        local_time = local_accum(trial_idx,1).time;
        min_time_idx = min(min_time_idx, length(local_time));
    end
end

for field_idx=1:length(accum_field_list)
    field_name = accum_field_list{field_idx};
    local_accum = grand_accum.(field_name);
    
    if isempty(local_accum)
        continue;
    end
    
    prop_field_list = fieldnames(local_accum);
    
    for trial_idx=1:size(local_accum,1)
        for subr_idx=1:size(local_accum,2)
            for prop_idx=1:length(prop_field_list)
                prop_name = prop_field_list{prop_idx};
                
                local_vals = local_accum(trial_idx, subr_idx).(prop_name);
                local_vals = local_vals(1:min_time_idx);
                local_accum(trial_idx, subr_idx).(prop_name) = local_vals;
            end
        end
    end

    grand_accum.(field_name) = local_accum;
end


end

function grand_accum = addToAccumArray(grand_accum, accum_name, ...
    time, kine, kine_row)
% This function appends a time and values array to the local_accum
% cell array. The local_accum array must be a 2xN array, and N will
% be increased as necessary to accomodate new elements. The input
% kine must be a kine struct from lpg_analyzeLarvaTrajectoriesTimeDiv,
% and kine_row specifies the row to select.

field_name_list = fieldnames(kine);
local_accum = grand_accum.(accum_name);
next_idx = size(local_accum,1) + 1;

for subr_idx=1:size(kine,2)
    local_accum(next_idx,subr_idx).time = time;
end

for field_idx=1:length(field_name_list)
    field_name = field_name_list{field_idx};  
    
    for subr_idx=1:size(kine,2)
        vals = kine(kine_row, subr_idx).(field_name);
        local_accum(next_idx,subr_idx).(field_name) = vals;
    end
end

grand_accum.(accum_name) = local_accum;

end



