function runDisplay()

    cfg.screen_size = [608,684]; % DLP3000 native resolution
    cfg.client_size = [1216,684]; % scale to physical dimensions of DMD
    cfg.screen_id = 1;
    cfg.serial_file = '/dev/ttyUSB0';
    cfg.serial_baudrate = 115200;
    cfg.serial_parity = 'none';
    cfg.serial_stopbits = 1;
    cfg.serial_terminator = 'LF';
    cfg.serial_frame_skipping = 5;
    
    cfg.disk_positions = [...
        315, 200; ...
        590, 200; ...
        865, 200; ...
        315, 470; ...
        590, 470; ...
        865, 470];
    cfg.disk_orientations = [...
        -45;
        -45;
        45;
        225;
        135;
        135];    
    cfg.disk_diameter = 270;
    
    cfg.test_color = [0, 0, 1];
    cfg.alt_color = [1, 0, 0];
    
    % initialize serial port
    serial_handle = serial(cfg.serial_file, ...
        'BaudRate', cfg.serial_baudrate, ...
        'ByteOrder', 'littleEndian', ...
        'DataBits', 8, ...
        'Parity', cfg.serial_parity, ...
        'StopBits', cfg.serial_stopbits, ...
        'Terminator', cfg.serial_terminator);
    
    fopen(serial_handle);
    serial_handle.ReadAsyncMode = 'continuous';
    
    % initialize psychtoolbox
    PsychDefaultSetup(2);

    window = PsychImaging('OpenWindow', cfg.screen_id, ...
        [0, 0, 0], [0, 0, cfg.screen_size], [], [], [], [], [], [],...
        [0, 0, cfg.client_size]);

    start_time = Screen('Flip', window);
    
    % enter the main event loop
    cur_pattern = 0;
    
    serial_frame_idx = 0;
    while true        
        % if there is a new command in the buffer 
        if serial_frame_idx == 0 && serial_handle.BytesAvailable > 0
            cmd_line = fgetl(serial_handle);
            
            switch cmd_line(1)
                case 'p'
                    first_space = find(cmd_line(2:end) == ' ', 1, 'first');
                    if isempty(first_space)
                        pattern_args = [];
                        cur_pattern = str2double(cmd_line(2:end));
                    else
                        [pattern_args,conv_ok] = str2num(cmd_line(1+first_space:end));
                        if ~conv_ok
                            error 'invalid input command';
                        end
                        
                        cur_pattern = str2double(cmd_line(2:first_space));
                    end
                    
                    if isnan(cur_pattern)
                        error 'invalid input command';
                    end
                    
                case 'x'
                    break;
                otherwise
                    error 'invalid input command';
            end
            
            pattern_start_time = cur_time;
        end
        
        % NOTE: "left" and "right" are arbitrarily defined here,
        % may or may not match with "left" and "right" in 
        % lpg_analyze* code
        
        % if not, then display the current pattern
        switch cur_pattern
            case 0
                % black everywhere
            case 1
                % blue everywehre
                showFullDisks(window, cfg.disk_positions, ...
                    cfg.disk_diameter, cfg.test_color);
            case 2
                % black on left, blue on right
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.test_color, 'R');
            case 3
                % black on right, blue on left
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.test_color, 'L');
            case 4
                % full field flash
                % args(1) is flash frequency
                rel_time = cur_time - pattern_start_time;
                cycle_time = rel_time - (1/pattern_args(1)) * ...
                    floor(rel_time * pattern_args(1));
            
                if cycle_time * pattern_args(1) < 0.5
                    % blue everywehre
                    showFullDisks(window, cfg.disk_positions, ...
                        cfg.disk_diameter, cfg.test_color);
                else
                    % black everywhere
                end
            case 5
                % red on left, blue on right
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.alt_color, 'L');
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.test_color, 'R');
            case 6
                % red on right, blue on left
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.alt_color, 'R');
                showHalfDisks(window, cfg.disk_positions, ...
                    cfg.disk_orientations, cfg.disk_diameter, ...
                    cfg.test_color, 'L');
            case 7
                % flash red on left, blue on right
                % args(1) is flash frequency
                rel_time = cur_time - pattern_start_time;
                cycle_time = rel_time - (1/pattern_args(1)) * ...
                    floor(rel_time * pattern_args(1));
            
                if cycle_time * pattern_args(1) < 0.5
                    % red on left, blue on right
                    showHalfDisks(window, cfg.disk_positions, ...
                        cfg.disk_orientations, cfg.disk_diameter, ...
                        cfg.alt_color, 'L');
                    showHalfDisks(window, cfg.disk_positions, ...
                        cfg.disk_orientations, cfg.disk_diameter, ...
                        cfg.test_color, 'R');
                else
                    % black everywhere
                end
            case 8
                % flash red on right, blue on left
                % args(1) is flash frequency
                rel_time = cur_time - pattern_start_time;
                cycle_time = rel_time - (1/pattern_args(1)) * ...
                    floor(rel_time * pattern_args(1));
            
                if cycle_time * pattern_args(1) < 0.5
                    % red on right, blue on left
                    showHalfDisks(window, cfg.disk_positions, ...
                        cfg.disk_orientations, cfg.disk_diameter, ...
                        cfg.alt_color, 'R');
                    showHalfDisks(window, cfg.disk_positions, ...
                        cfg.disk_orientations, cfg.disk_diameter, ...
                        cfg.test_color, 'L');
                else
                    % black everywhere
                end
            otherwise
                error 'unrecognized pattern';
        end
        
        cur_time = Screen('Flip', window, 0) - start_time;
        serial_frame_idx = mod(serial_frame_idx + 1, cfg.serial_frame_skipping);
    end
    
    Screen('CloseAll');
    fclose(serial_handle);
    delete(serial_handle);
end

function showHalfDisks(window, disk_positions, disk_orientations, ...
    disk_diameter, disk_color, disk_side)
% This function displays each of the disks using the passed
% positions (Nx2), orientations (Nx1), diameter, color (1x3),
% and side ('L' or 'R')

for disk_idx=1:size(disk_positions, 1)
    disk_x = disk_positions(disk_idx,1);
    disk_y = disk_positions(disk_idx,2);
    
    disk_rect = [...
        disk_x - 0.5*disk_diameter, ...
        disk_y - 0.5*disk_diameter, ...
        disk_x + 0.5*disk_diameter, ...
        disk_y + 0.5*disk_diameter];
    
    if disk_side == 'L'
        angle_start = 180 + disk_orientations(disk_idx);
    elseif disk_side == 'R'
        angle_start = 0 + disk_orientations(disk_idx);
    else
        error 'internal error';
    end
    
    Screen('FillArc', window, disk_color, disk_rect, ...
        angle_start, 180);
end

end

function showFullDisks(window, disk_positions, ...
    disk_diameter, disk_color)
% This function displays each of the disks using the passed
% positions (Nx2), orientations (Nx1), diameter, and color (1x3)

for disk_idx=1:size(disk_positions, 1)
    disk_x = disk_positions(disk_idx,1);
    disk_y = disk_positions(disk_idx,2);
    
    disk_rect = [...
        disk_x - 0.5*disk_diameter, ...
        disk_y - 0.5*disk_diameter, ...
        disk_x + 0.5*disk_diameter, ...
        disk_y + 0.5*disk_diameter];
    
    Screen('FillArc', window, disk_color, disk_rect, 0, 360);
end

end


