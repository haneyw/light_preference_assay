function plotOnHour(accum)
% This function plots the peak values for the eshock experiments

global_t_min = Inf;
global_t_max = -Inf;
mm_to_pixels = 12.5950;

for indiv_idx=1:size(accum,1)
    t = accum(indiv_idx,1).time;
    
    y_sub1 = accum(indiv_idx,1).trans_vel .* accum(indiv_idx,1).total_time;
    y_sub2 = accum(indiv_idx,2).trans_vel .* accum(indiv_idx,2).total_time;
    
    y_sub1(isnan(y_sub1)) = 0;
    y_sub2(isnan(y_sub2)) = 0;
    
    y = (y_sub1 + y_sub2) ./ (accum(indiv_idx,1).total_time + accum(indiv_idx,2).total_time);
    y = y / mm_to_pixels;
    
    t = t/60;
    
    t_min = ceil(min(t));
    t_max = floor(max(t));

    strikes = zeros(t_max - t_min + 1, 1);

    for t_idx=t_min:t_max
        t_diff = abs(t - t_idx);
        t_rnd = t_diff == min(t_diff);

        strikes(t_idx - t_min + 1) = y(t_rnd);
    end
    
    local_accum(indiv_idx).t_min = t_min;
    local_accum(indiv_idx).strikes = strikes;
    
    global_t_min = min(global_t_min, t_min);
    global_t_max = max(global_t_max, t_max);
end

new_t_base = global_t_min:global_t_max;
new_strikes = zeros(size(accum,1), length(new_t_base));

for indiv_idx=1:size(accum,1)
   t_off = local_accum(indiv_idx).t_min - global_t_min +1;
   strikes = local_accum(indiv_idx).strikes;
   
   new_strikes(indiv_idx, t_off:t_off+length(strikes)-1) = strikes;
end

y_med = median(new_strikes, 1, 'omitnan');
y_n = sum(~isnan(new_strikes), 1);
y_sem = std(new_strikes, 1, 1, 'omitnan') ./ sqrt(y_n);

plot(new_t_base, y_med + y_sem, 'r');
hold on;
plot(new_t_base, y_med - y_sem, 'r');
hold on;
plot(new_t_base, y_med, 'k');

end