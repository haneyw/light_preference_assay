    function props = getPlateProps(protocol_name)
% This function returns the properties of the well plates for each 
% protocol.
%   
% The function recieves the following inputs:
%   protocol_name - the name of the protocol you wish to get the wellplate
%       properties for. 
%
% The function has the following outputs:
%   plate_dims - a 1x2 array that specifies the number of rows and
%       columns of well plate
%   mask_type - a string that specifies the mask type. allowed values
%       are 'circular' and 'clipped_circular'
%   mask_num_clips - a scalar value indicating the number of clip lines.
%       ignored unless mask_type is 'clipped_circular'
%   subregions_type - a string that specifies the subregion type, or 
%       empty if no subregions. allowed values are 'vertical', 'vector',
%       and 'vector_file'
%   num_stages - a scalar indicating the number of stages in the protocol
%   stage_start_time - a 1xN array that specifies the nominal start time
%       of each stage
%   heartbeat - a 1xN array that specifies the period of the heartbeat
%       for each each of N stages. The value if NaN if no heartbeat exists


switch protocol_name
    case {'top_bottom', 'eshock_1sec', 'eshock_2sec', 'two_hour_movement'...
            , 'eshock_1volt', 'eshock_6volt', 'eshock_12volt', 'eshock_1volt_3sec', 'eshock_6volt_3sec'...
            , 'eshock_12volt_3sec', 'eshock_1volt_vtest'...
            , 'eshock_3volt_vtest', 'eshock_6volt_vtest', 'eshock_9volt_vtest'...
            , 'eshock_12volt_vtest', 'eshock_5sec', 'eshock_3sec',...
            'eshock_7sec', 'no_feed'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'none'};
        props.heartbeat_expect = {[]};
    case {'eshock_3ma', 'eshock_6ma'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'period'};
        props.heartbeat_expect = {60};
    case {'size_55mm_control', 'size_55mm_bottom', 'size_55mm_top'}
        props.plate_dims = [1,2];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'none'};
        props.heartbeat_expect = {[]};
    case {'size_75mm_control', 'size_75mm_bottom', 'size_75mm_top'}
        props.plate_dims = [1,1];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'none'};
        props.heartbeat_expect = {[]};
    case {'top_bottom_AITC_white', 'top_bottom_AITC_black', 'bottom_AITC'...
            'control_AITC','top_AITC'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical', 'vertical'};
        props.num_stages = 2;
        props.stage_start_time = [0; 360]; % adding AITC takes ~60s
        props.heartbeat_type = {'none', 'none'};
        props.heartbeat_expect = {[],[]};
    case {'bottom_proj','bottom_proj_v2', 'bottom_proj_overhead','flash',...
            'flash_20sec','bottom_proj_rb'}
        props.plate_dims=[2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'none'};
        props.heartbeat_expect = {[]};
    case {'celatum_prelim0_e3', 'celatum_prelim0_media', 'celatum_prelim0_exper'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'none'};
        props.heartbeat_expect = {[]};
    case {'celatum_prelim1_e3', 'celatum_prelim1_media', 'celatum_prelim1_exper'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical', 'vertical'};
        props.num_stages = 2;
        props.stage_start_time = [0; 360]; % adding extract takes ~60s
        props.heartbeat_type = {'none', 'none'};
        props.heartbeat_expect = {[],[]};
    case {'flash_20sec_hb'}
        props.plate_dims=[2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'times'};
        props.heartbeat_expect = {(0:19)*60 + 60*5};
    case {'flash_30sec_4min'}
        props.plate_dims=[2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'times'};
        props.heartbeat_expect = {(0:9)*270 + 60*5};
    case {'flash_2min_2min'}
        props.plate_dims=[2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'times'};
        props.heartbeat_expect = {(0:9)*240 + 60*5};
    case {'flash_2min_8min', 'flash_2min_8min_lf', 'flash_2min_8min_rb'}
        props.plate_dims=[2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 1;
        props.stage_start_time = 0;
        props.heartbeat_type = {'times'};
        props.heartbeat_expect = {(0:9)*600 + 60*5};
    case {'AITC_rb'}
        props.plate_dims = [2,3];
        props.mask_type = 'clipped_circular';
        props.mask_num_clips = 2;
        props.subregions_type = {'vector_file','vector_file'};
        props.subregions = {'flow_diag_2019_01_29_subr.mat','flow_diag_2019_01_29_subr.mat'};
        props.num_stages = 2;
        props.stage_start_time = [0; 360]; % adding AITC takes ~60s
        props.heartbeat_type = {'none', 'none'};
        props.heartbeat_expect = {[],[]};
    case {'AITC_meto','AITC_pb','AITC_drugs','AITC_drugs2'}
        props.plate_dims = [2,3];
        props.mask_type = 'circular';
        props.mask_num_clips = NaN;
        props.subregions_type = {'vertical', 'vertical','vertical'};
        props.num_stages = 3;
        props.stage_start_time = [0; 660; 3420]; % adding compounds takes ~60s each time
        props.heartbeat_type = {'none', 'none','none'};
        props.heartbeat_expect = {[],[],[]};
    otherwise
        error 'unrecognized protocol name';
end

end
