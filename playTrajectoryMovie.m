function playTrajectoryMovie(req_date_name, req_stage_idx, req_trial_idx)
% This code plots the raw trajectories. It accepts the following
% parameters:
%   req_date_name - requested date name as a string
%   req_stage_idx - requested stage index as a scalar
%   req_trial_idx - requested trial index as a scalar

exper_params = getExperimentParams();

for setup_idx=1:size(exper_params,1)
    date_name = exper_params{setup_idx,1};
    trial_num = exper_params{setup_idx,2};
    animal_age = exper_params{setup_idx,3};
    protocol_name = exper_params{setup_idx,4};
    
    if ~(strcmp(date_name, req_date_name) && ...
           trial_num == req_trial_idx)
        continue;
    end
    
    props = getPlateProps(protocol_name);
    
    raw_data_folder = [getDataFolder(), filesep, ...
        'raw_data', filesep, date_name];
    analysis_folder = [getDataFolder(), filesep, ...
        'analysis', filesep, date_name];

    if props.num_stages == 1
        vid_fname = [raw_data_folder, filesep, ...
            sprintf('trial%04d.ufmf', trial_num)];
        traj_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_traj.mat', trial_num)];
    else
        vid_fname = [raw_data_folder, filesep, ...
            sprintf('trial%04d_stage%02d.ufmf', trial_num, req_stage_idx)];
        traj_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_stage%02d_traj.mat', trial_num, req_stage_idx)];
    end
    
    load(traj_fname, 'time', 'raw_pos', 'xcorr_signal');
    
    
    % these parameters should be matched to aggregateTrials values
    analyze_cfg.snr_high = 0.5;
    analyze_cfg.snr_med = 0.4;
    analyze_cfg.allow_dist_error = 10; % pixels
    
    usable_mask = zeros(size(xcorr_signal));
    for area_idx=1:size(usable_mask,1)
        usable_mask(area_idx,:) = lpg_findUsableTrajectoryPoints(time, ...
            squeeze(raw_pos(1,area_idx,:)), ...
            squeeze(raw_pos(2,area_idx,:)), ...
            xcorr_signal(area_idx,:)', ...
            analyze_cfg);
    end
    
    nia_playUFMFTrajMovie(vid_fname, raw_pos, usable_mask);
end


end

