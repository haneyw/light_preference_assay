function cams = startTopBottomExperiment(protocol_name, trial_num, stage_num)
% STARTEXPERIMENT Runs cameras and mass flow controllers through
%   specified protocols.
%   This function starts and records videos of the larval proving ground
%   experiments. The input is the desired protocol and username, which are
%   used to select the specific set of experimental parameters and create a
%   folder under the operator's name to store all of the videos acquired.
%   The function starts the cameras and mass flow controllers, acquires
%   videos, and loops through the specific set of experimental parameters.
%
%   This function has the following outputs:
%      cams - Mandatory 1xN structure array with the following fields:
%         dev_id - Device ID (integer)
%         mode - Camera capture mode (integer)
%         offsetX - ROI X offset (integer)
%         offsetY - ROI Y offset (integer)
%         width - ROI width (integer)
%         height - ROI height (integer)
%         framerate - Percent of maximum (0 to 100, floating-point)
%         pixel_format - Pixel format ('mono8', or 'raw8')
%         frames_per_trigger - Frames per trigger (integer)
%         start_paused - Start camera in paused mode (0 or 1)
%         trigger_enabled - Set trigger enabled (0 or 1)
%         trigger_mode - Trigger mode (integer)
%         trigger_source - Trigger source (gpio pin, integer)
%         trigger_polarity - Trigger polarity (0 for low or 1 for high)
%         strobe_enabled - Set strobe enabled (0 or 1)
%         strobe_source - Strobe output (gpio pin, integer)
%         strobe_polarity - Strobe polarity (0 for low or 1 for high)
%         output_file - Output file name to save data
%         callback_period - Callback period (integer)

if nargin < 3
    stage_num = [];
end

switch protocol_name
    
    case 'size_75mm_bottom'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
   
    case 'size_75mm_control'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
    
    case 'size_75mm_top'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
   
    case 'size_55mm_bottom'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
    
    case 'size_55mm_control'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
    
    case 'size_55mm_top'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
   
    case 'top_bottom'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 8;
    
    case 'top_bottom_AITC_white'
        if isempty(stage_num)
            error 'stage is required';
        end
        
        switch stage_num
            case 1
                sample_time = 60 * 5;
            case 2
                sample_time = 60 * 30;
            otherwise
                error 'unrecognized stage value';
        end
        
        shutter_time = 8;
    
    case 'top_bottom_AITC_black'
        if isempty(stage_num)
            error 'stage is required';
        end
        
        switch stage_num
            case 1
                sample_time = 60 * 5;
            case 2
                sample_time = 60 * 30;
            otherwise
                error 'unrecognized stage value';
        end
        
        shutter_time = 8;
    
    case 'eshock_3ma'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 35;
        shutter_time = 8;
   
    case 'eshock_6ma'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 35;
        shutter_time = 8;
   
    case 'bottom_proj'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 10;
        
    case 'bottom_proj_v2'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 10;
    
    case 'bottom_proj_rb'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        sample_time = 60 * 30;
        shutter_time = 10;
    
    case {'bottom_AITC','control_AITC','top_AITC'}
        if isempty(stage_num)
            error 'stage is required';
        end
        
        switch stage_num
            case 1
                sample_time = 60 * 5;
            case 2
                sample_time = 60 * 30;
            otherwise
                error 'unrecognized stage value';
                
        end
        shutter_time = 8;
    otherwise
        error 'unrecognized protocol name';
end



data_path = 'D:\AustinHaney\data\raw_data';

% configure the cameras

cam_cfg.cameras(1).dev_id = 0;
cam_cfg.cameras(1).mode = 0;
cam_cfg.cameras(1).offsetX = 242;
cam_cfg.cameras(1).offsetY = 464;
cam_cfg.cameras(1).width = 1600;
cam_cfg.cameras(1).height = 1082;
cam_cfg.cameras(1).framerate = 18;
cam_cfg.cameras(1).pixel_format = 'raw8';
cam_cfg.cameras(1).frames_per_capture = -1;
cam_cfg.cameras(1).start_paused = 0;
cam_cfg.cameras(1).trigger_enabled = 0;
cam_cfg.cameras(1).trigger_mode = 0;
cam_cfg.cameras(1).trigger_source = 0;
cam_cfg.cameras(1).strobe_enabled = 0;
cam_cfg.cameras(1).callback_period = 2;
cam_cfg.cameras(1).shutter_auto = 0;
cam_cfg.cameras(1).shutter_value = shutter_time;
cam_cfg.cameras(1).gain_auto = 0;
cam_cfg.cameras(1).gain_value = 0;
cam_cfg.cameras(1).output_type = 'ufmf';
cam_cfg.cameras(1).output_file = '';

% create the directory for the data
folder_name = [data_path, filesep, getDateFolder()];
if ~exist(folder_name, 'file')
    mkdir(folder_name);
end

% create the trial name
if isempty(stage_num)
    ufmf_name = sprintf('%s%strial%04d.ufmf', folder_name, filesep, trial_num);
    config_name = sprintf('%s%strial%04d_config.mat', folder_name, filesep, trial_num);
else
    ufmf_name = sprintf('%s%strial%04d_stage%02d.ufmf', folder_name, filesep, trial_num, stage_num);
    config_name = sprintf('%s%strial%04d_stage%02d_config.mat', folder_name, filesep, trial_num, stage_num);
end

if exist(ufmf_name, 'file') || exist(config_name, 'file')
    error 'output file already exists';
end

cam_cfg.cameras(1).output_file = ufmf_name;

% update experimental configuration
exper_cfg.sample_time = sample_time;
exper_cfg.cam_cfg = cam_cfg;
exper_cfg.protocol_name = protocol_name;

save(config_name, 'exper_cfg');

% run the protocol
cams = lpg_PtGreyStartCameras(cam_cfg);

is_term = false;
start_time = tic;
while ~is_term
    elapsed_time = toc(start_time);
    if elapsed_time > sample_time
        break;
    end
    
    is_term = lpg_PtGreyUpdateDisplay(cams);
    drawnow limitrate;
end

if ~is_term
    lpg_PtGreyStartSlowStopCameras(cams);

    while ~lpg_PtGreySlowStopCamerasIsDone(cams)
        drawnow limitrate;
    end

    lpg_PtGreyFinishSlowStopCameras(cams);
end

close(cams.preview_figure);

end

 
function folder_name = getDateFolder()

date_nums = datevec(now);

folder_name = sprintf('%04d_%02d_%02d', ...
  date_nums(1), date_nums(2), date_nums(3));

end