function rerunTraj()

params = getExperimentParams();
date = unique(params(:,1));

for trial = 1:length(date)
    disp(['Running ', date{trial}]);
    computeTraj(date{trial});
end

end

