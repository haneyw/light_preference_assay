function folder = getDataFolder()
% This function returns the folder that contains the raw
% and analyzed data.

%pavlov on windows
%folder = 'E:\Austin\data';

%laptop
%folder = 'D:\Documents\PHD\data';

%pavlov on linux desktop in lab
folder = '/run/media/lmember/Pavlov2/AustinHaney/data';

end
