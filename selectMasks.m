function selectMasks(date_name)
% This function compiles the necesary inputs for lpg_getWellPlatemasks 
% it accepts the following inputs 
%   date_name - A string that is the name of the folder you wish to calculate
%               masks for.

raw_data_folder = [getDataFolder(), filesep, ...
    'raw_data', filesep, date_name];
analysis_folder = [getDataFolder(), filesep, ...
    'analysis', filesep, date_name];

if ~exist(analysis_folder,'dir')
    mkdir(analysis_folder);
end

exper_params = getExperimentParams();

for idx=1:size(exper_params,1)    
    if ~strcmp(exper_params{idx,1}, date_name)
        continue;
    end
    
    trial_num = exper_params{idx,2};
    protocol_name = exper_params{idx,4};
    
    props = getPlateProps(protocol_name);

    % add stage to filename if necessary
    if props.num_stages == 1
       vid_fname = [raw_data_folder, filesep, ...
           sprintf('trial%04d.ufmf', trial_num)];
       mask_fname = [analysis_folder, filesep, ...
           sprintf('trial%04d_mask.mat', trial_num)];
       heartm_fname = [analysis_folder, filesep, ...
           sprintf('trial%04d_heart_mask.mat', trial_num)];
       subr_fname = [analysis_folder, filesep, ...
           sprintf('trial%04d_subr.mat', trial_num)];
       
       selectMasksForTrial(props, 1, vid_fname, mask_fname, ...
           heartm_fname, subr_fname);       
    else
       for stage_idx=1:props.num_stages
          vid_fname = [raw_data_folder, filesep, ...
              sprintf('trial%04d_stage%02d.ufmf', trial_num, stage_idx)];
          mask_fname = [analysis_folder, filesep, ...
              sprintf('trial%04d_stage%02d_mask.mat', trial_num, stage_idx)];
          heartm_fname = [analysis_folder, filesep, ...
              sprintf('trial%04d_stage%02d_heart_mask.mat', trial_num, stage_idx)];
          subr_fname = [analysis_folder, filesep, ...
              sprintf('trial%04d_stage%02d_subr.mat', trial_num, stage_idx)];
          
          selectMasksForTrial(props, stage_idx, vid_fname, mask_fname, ...
              heartm_fname, subr_fname);
       end
    end
end

end

function selectMasksForTrial(props, stage_idx, vid_fname, mask_fname, ...
    heartb_fname, subr_fname)

switch props.mask_type
    case 'circular'
        lpg_getWellPlateMasks(vid_fname, mask_fname, ...
            props.plate_dims(1), props.plate_dims(2));
    case 'clipped_circular'
        lpg_getClippedWellPlateMasks(vid_fname, mask_fname, ...
            props.plate_dims(1), props.plate_dims(2), props.mask_num_clips);
    otherwise
        error 'unregonized mask type';
end

if ~strcmp(props.heartbeat_type{stage_idx}, 'none')
    lpg_getHeartbeat(vid_fname, heartb_fname);
end

analysis_folder = [getDataFolder(), filesep, 'analysis'];

if ~isempty(props.subregions_type{stage_idx})
    switch props.subregions_type{stage_idx}
        case 'vertical'
            lpg_getWellPlateSubregionsVertical(vid_fname, mask_fname, subr_fname);
        case 'vector'
            lpg_getWellPlateSubregionsVector(vid_fname, mask_fname, subr_fname);
        case 'vector_file'
            old_name = [analysis_folder, filesep, props.subregions{stage_idx}];
            copyfile(old_name, subr_fname);
        otherwise
            error 'unrecognized subregion type';
    end
end

end
