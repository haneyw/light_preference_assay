function plotTrajectories(req_date_name, req_stage_idx, req_trial_idx, req_time)
% This code plots the raw trajectories. It accepts the following
% parameters:
%   req_date_name - requested date name as a string
%   req_stage_idx - requested stage index as a scalar
%   req_trial_idx - requested trial index as a scalar
%   req_time - requested time range as [1x2] matrix, or empty
%       to plot all time points

cfg.cutoff_freq = 5;
cfg.min_stretch = 15;
cfg.min_snr = 0.5;
cfg.color = [0, 0, 0, 0.1];

if isempty(req_stage_idx)
    error 'req_stage_idx must have value';
end

exper_params = getExperimentParams();

for setup_idx=1:size(exper_params,1)
    date_name = exper_params{setup_idx,1};
    trial_num = exper_params{setup_idx,2};
    animal_age = exper_params{setup_idx,3};
    protocol_name = exper_params{setup_idx,4};
    
    if ~(strcmp(date_name, req_date_name) && ...
           trial_num == req_trial_idx)
        continue;
    end
    
    props = getPlateProps(protocol_name);
    
    analysis_folder = [getDataFolder(), filesep, ...
        'analysis', filesep, date_name];

    if props.num_stages == 1
        traj_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_traj.mat', trial_num)];
        heartb_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_heart_beat.mat', trial_num)];
    else
        traj_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_stage%02d_traj.mat', trial_num, req_stage_idx)];
        heartb_fname = [analysis_folder, filesep, ...
            sprintf('trial%04d_stage%02d_heart_beat.mat', trial_num, req_stage_idx)];
    end
    
    load(traj_fname, 'raw_pos', 'xcorr_signal', 'time');
    
    if ~strcmp(props.heartbeat_type{req_stage_idx}, 'none')
        load(heartb_fname, 'heartbeat');
        time = lpg_applyHeartBeat(time, ...
            props.heartbeat_type{req_stage_idx}, ...
            props.heartbeat{req_stage_idx}, heartbeat);
    end
    
    if ~isempty(req_time)
        time_mask = time >= req_time(1) & time < req_time(2);
    else
        time_mask = ones(length(time),1, 'logical');
    end
    
    fig = figure;
    ax = axes(fig);
    
    for well_idx = 1:size(raw_pos,2)
        x_well = squeeze(raw_pos(1,well_idx,time_mask));
        y_well = squeeze(raw_pos(2,well_idx,time_mask));
        snr_well = xcorr_signal(well_idx,time_mask);
        
        % find contiguous stretches
        stretches = lpg_findTrueStretches(snr_well > cfg.min_snr, cfg.min_stretch);
        
        % analyze each contiguous stretch
        for stretch_idx=1:size(stretches,2)
            stretch_first = stretches(1,stretch_idx);
            stretch_last = stretches(2,stretch_idx);
            
            x_stretch = x_well(stretch_first:stretch_last);
            y_stretch = y_well(stretch_first:stretch_last);

            h = plot(ax, x_stretch, y_stretch, 'LineWidth', 2, 'Color', 'k');
            h.Color = cfg.color;
            ax.YDir = 'reverse';
            hold(ax, 'on');
            axis(ax, 'equal');
        end

    end
    
end


end

