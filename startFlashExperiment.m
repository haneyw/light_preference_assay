function cams = startFlashExperiment(protocol_name, trial_num, stage_num)
% STARTFLASHEXPERIMENT Runs specified protocol.
%   This function starts and records videos of the larval proving ground
%   experiments. The input is the desired protocol, trial number, and 
%   stage number.
%

if nargin < 3
    stage_num = [];
end

debug = true;

exper_cfg.protocol_name = protocol_name;

switch protocol_name
    case 'flash'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        exper_cfg.base_pattern = 2;
        exper_cfg.flash_pattern = 4;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 20;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 5;
        exper_cfg.rest_time = 55;
        exper_cfg.lagging_time = 10 * 60;
        
    case 'flash_20sec'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        exper_cfg.base_pattern = 2;
        exper_cfg.flash_pattern = 4;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 20;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 20;
        exper_cfg.rest_time = 40;
        exper_cfg.lagging_time = 10 * 60;
        
    case 'flash_30sec_4min'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        exper_cfg.base_pattern = 2;
        exper_cfg.flash_pattern = 4;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 10;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 30;
        exper_cfg.rest_time = 60*4;
        exper_cfg.lagging_time = 10 * 60;
        
        
    case 'flash_2min_2min'
        if ~isempty(stage_num)
            error 'stage is not used';
        end
        
        exper_cfg.base_pattern = 2;
        exper_cfg.flash_pattern = 4;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 10;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 60*2;
        exper_cfg.rest_time = 60*2;
        exper_cfg.lagging_time = 10 * 60;
        
    case 'flash_2min_8min'
        if ~isempty(stage_num)
            error 'stage is not used';
        end

        exper_cfg.base_pattern = 2;
        exper_cfg.flash_pattern = 4;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 10;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 60*2;
        exper_cfg.rest_time = 60*8;
        exper_cfg.lagging_time = 15 * 60;
        
    case 'flash_2min_8min_rb'
        if ~isempty(stage_num)
            error 'stage is not used';
        end

        exper_cfg.base_pattern = 5;
        exper_cfg.flash_pattern = 7;
        exper_cfg.leading_time = 5 * 60;
        exper_cfg.num_flash_cycles = 10;
        exper_cfg.flash_freq = 10;
        exper_cfg.flash_time = 60*2;
        exper_cfg.rest_time = 60*8;
        exper_cfg.lagging_time = 15 * 60;
 
    otherwise
        
        error 'unrecognized protocol';
end

data_path = 'D:\AustinHaney\data\raw_data';

% configure the cameras

cam_cfg.cameras(1).dev_id = 0;
cam_cfg.cameras(1).mode = 0;
cam_cfg.cameras(1).offsetX = 242;
cam_cfg.cameras(1).offsetY = 464;
cam_cfg.cameras(1).width = 1600;
cam_cfg.cameras(1).height = 1082;
cam_cfg.cameras(1).framerate = 18;
cam_cfg.cameras(1).pixel_format = 'raw8';
cam_cfg.cameras(1).frames_per_capture = -1;
cam_cfg.cameras(1).start_paused = 0;
cam_cfg.cameras(1).trigger_enabled = 0;
cam_cfg.cameras(1).trigger_mode = 0;
cam_cfg.cameras(1).trigger_source = 0;
cam_cfg.cameras(1).strobe_enabled = 0;
cam_cfg.cameras(1).callback_period = 2;
cam_cfg.cameras(1).shutter_auto = 0;
cam_cfg.cameras(1).shutter_value = 8;
cam_cfg.cameras(1).gain_auto = 0;
cam_cfg.cameras(1).gain_value = 0;
cam_cfg.cameras(1).output_type = 'ufmf';
cam_cfg.cameras(1).output_file = '';

% create the directory for the data
folder_name = [data_path, filesep, getDateFolder()];
if ~exist(folder_name, 'file')
    mkdir(folder_name);
end

% create the trial name
if isempty(stage_num)
    ufmf_name = sprintf('%s%strial%04d.ufmf', folder_name, filesep, trial_num);
    config_name = sprintf('%s%strial%04d_config.mat', folder_name, filesep, trial_num);
else
    ufmf_name = sprintf('%s%strial%04d_stage%02d.ufmf', folder_name, filesep, trial_num, stage_num);
    config_name = sprintf('%s%strial%04d_stage%02d_config.mat', folder_name, filesep, trial_num, stage_num);
end

if exist(ufmf_name, 'file') || exist(config_name, 'file')
    error 'output file already exists';
end

cam_cfg.cameras(1).output_file = ufmf_name;

% update experimental configuration
exper_cfg.cam_cfg = cam_cfg;

save(config_name, 'exper_cfg');

% start the DAQ system
daq_sess = daq.createSession('ni');
daq_sess.addDigitalChannel('Dev1', 'Port0/Line0', 'OutputOnly');
daq_sess.addDigitalChannel('Dev1', 'Port0/Line1', 'OutputOnly');
daq_sess.outputSingleScan([0, 1]);

% open the display serial line
serial_handle = serial('COM3', ...
    'BaudRate', 115200, ...
    'ByteOrder', 'littleEndian', ...
    'DataBits', 8, ...
    'Parity', 'none', ...
    'StopBits', 1, ...
    'Terminator', 'LF');
fopen(serial_handle);

% start the screen on pattern 
setPattern(serial_handle, exper_cfg.base_pattern);
pause(1)

% wait for user to give the all clear
input('Load animals then hit any key to continue', 's');

% run the protocol
cams = lpg_PtGreyStartCameras(cam_cfg);

% Run through leading stage
if debug
    disp('Starting leading stage');
end

is_term = false;
start_time = tic;
while ~is_term
    rel_time = toc(start_time);
    if rel_time > exper_cfg.leading_time
        break;
    end
    
    is_term = lpg_PtGreyUpdateDisplay(cams);    
    drawnow limitrate;
end

% Run through stimulus stages
flash_cycle_time = exper_cfg.flash_time + exper_cfg.rest_time;
for cycle_idx=1:exper_cfg.num_flash_cycles
    if is_term
        break;
    end
    
    cycle_start_time = exper_cfg.leading_time + ...
        (cycle_idx-1)*flash_cycle_time;

    if debug
        disp(['Starting cycle ', num2str(cycle_idx), ' flash']);
    end
    
    setPattern(serial_handle, exper_cfg.flash_pattern, exper_cfg.flash_freq);
    daq_sess.outputSingleScan([1, 1]);
    
    while ~is_term
        rel_time = toc(start_time) - cycle_start_time;
        if rel_time > exper_cfg.flash_time
            break;
        end

        is_term = lpg_PtGreyUpdateDisplay(cams);
        drawnow limitrate;
    end
    
    if is_term
        break;
    end
    
    if debug
        disp(['Starting cycle ', num2str(cycle_idx), ' reset']);
    end
    
    setPattern(serial_handle, exper_cfg.base_pattern);
    daq_sess.outputSingleScan([0, 1]);
    
    while ~is_term
        rel_time = toc(start_time) - cycle_start_time;
        if rel_time > exper_cfg.flash_time + exper_cfg.rest_time
            break;
        end

        is_term = lpg_PtGreyUpdateDisplay(cams);
        drawnow limitrate;
    end
end

% Start lagging stage
if ~is_term
    lagging_start_time = exper_cfg.leading_time + ...
       exper_cfg.num_flash_cycles*flash_cycle_time;

    if debug
        disp('Starting lagging stage');
    end

    setPattern(serial_handle, exper_cfg.base_pattern);
    daq_sess.outputSingleScan([0, 1]);

    while ~is_term
        rel_time = toc(start_time) - lagging_start_time;
        if rel_time > exper_cfg.lagging_time
            break;
        end

        is_term = lpg_PtGreyUpdateDisplay(cams);
        drawnow limitrate;
    end
end
    

% all done, shut things down
if ~is_term
    lpg_PtGreyStartSlowStopCameras(cams);
    while ~lpg_PtGreySlowStopCamerasIsDone(cams)
        drawnow limitrate;
    end

    lpg_PtGreyFinishSlowStopCameras(cams);
end

close(cams.preview_figure);

daq_sess.outputSingleScan([0, 0]);
daq_sess.stop;
delete(daq_sess);

closeDisplay(serial_handle);
fclose(serial_handle);
delete(serial_handle);

end

function closeDisplay(serial_handle)

fwrite(serial_handle, uint8(['x', newline]));

end

function setPattern(serial_handle, pattern_id, pattern_args)

if nargin < 3
    pattern_args = [];
end

str = sprintf('p%d', pattern_id);

if ~isempty(pattern_args)
    args_str = [' ', num2str(pattern_args)];
else
    args_str = [];
end

fwrite(serial_handle, uint8([str, args_str, newline]));
   
end
 
function folder_name = getDateFolder()

date_nums = datevec(now);

folder_name = sprintf('%04d_%02d_%02d', ...
  date_nums(1), date_nums(2), date_nums(3));

end