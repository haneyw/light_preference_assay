This is a read me file that explains the pipline of analysis of ufmf videos for kinematic analysis

0) getExperimentParams: catalogs experimental protocols

1) selectMasks: calculates the masks for wach well plate. 
2) calculateBackground: calcultes background movie
3) computeTraj: calculates the trajectory and heartbeat signal

4a) plotTopBottomTSeries: plots time seris
4b) plotTrajectoryMovie: plots found coordinates on movie
4c) plotTrajectories: plots trajectory lines



